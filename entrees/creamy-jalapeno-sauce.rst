Creamy Jalapeno Sauce (v1.0)
============================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1/4 cup mayonnaise
-  2 teaspoons minced jalapeno slices
-  2 teaspoons juice from jalapeño slices
-  3/4 teaspoon sugar
-  1/2 teaspoon paprika
-  1/2 teaspoon cumin
-  1/8 teaspoon cayenne pepper
-  1/8 teaspoon garlic powder
-  dash salt

Procedure
---------

1. Mix all ingredients together in a bowl.

Notes
-----

Serving
^^^^^^^

Spread about 1 tablespoon over tortilla shell.

Use sauce on chicken quesadillas.
