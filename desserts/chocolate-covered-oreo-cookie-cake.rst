Chocolate Covered Oreo Cookie Cake (v1.0)
=========================================

| Created By: Unknown
| Created On: Unknown
| Servings: 16
| Prep. Time: Unknown
| Cook Time: 50 minutes

Ingredients
-----------

Cake
^^^^

-  1 package (2 layer size) devil's food chocolate cake mix

Glaze
^^^^^

-  4 squares Baker’s Semi- Sweet Baking Chocolate
-  1/4 cup butter, cut up

Filling
^^^^^^^

-  1 package (8 oz) cream cheese, softened
-  1/2 cup sugar
-  2 cups thawed cool whip
-  12 Oreos, coarsely crushed

Procedure
---------

Cake
^^^^

1. Preheat oven to 350 degrees F.
2. Prepare and bake cake mix as directed on package for 2 (9 inch) round
   cake pans.
3. Cool in pan 5 minutes.
4. Invert onto wire rack and remove pans. Cool Completely.

Glaze
^^^^^

1. Melt chocolate in small microwave bowl on HIGH for 1 to 2 minutes or
   until chocolate is melted, stirring after 30 second.
2. Blend in butter.
3. Set aside to slightly thicken, about 5 minutes.

Filling
^^^^^^^

1. Beat cream cheese and sugar in large bowl with electric mixer on
   medium speed until well blended.
2. Gently stir in whipped topping and crushed cookies.

Bringing It All Together
^^^^^^^^^^^^^^^^^^^^^^^^

1. Place 1 of the cake layers on serving plate, top side down.
2. Spread top of cake layer evenly with cream cheese mixture.
3. Place remaining cake layer on top, top side up. Spoon glaze to cover
   top of cake only.
