Rich & Charlie's Salad (v1.0)
=============================

| Created By: Unknown
| Created On: Unknown
| Servings: 8
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 head of Romaine lettuce
-  1 head of iceberg lettuce
-  1 (10oz) can hearts of palm, drained
-  1 (10oz) can artichoke hearts, drained, cut in half
-  1 (4oz) can diced pimentos
-  1 large red onion
-  1/2 cup parmesan cheese
-  1/2 cup oil
-  1/3 cup vinegar
-  2 tablespoon sugar
-  Fresh ground pepper and salt to taste

Procedure
---------

1. Toss the first seven ingredients: Romain lettuce, iceberg lettuce,
   hearts of palm, artichoke hearts, diced pimentos, red onion, and
   parmesan cheese.
2. In a separate container, mix oil, vinegar, and sugar to make
   dressing.
3. Pour dressing over salad.
4. Add salt and pepper to taste.
5. Toss again.
