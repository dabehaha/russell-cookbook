Macadamia Nut White Chocolate Chunk Cookies (v1.0)
==================================================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  3/4 cup firmly packed brown sugar
-  1/2 cup butter
-  1 egg
-  1-1/2 teaspoon vanilla
-  1 cups flour
-  1/2 teaspoon baking powder
-  1/2 teaspoon baking soda
-  1/2 teaspoon salt
-  2 (3oz) bars white chocolate cut into 1/2 inch pieces
-  1 (3.5 oz) jar salted macadamia nuts, coarsely chopped

Procedure
---------

1. Preheat oven to 350 degrees.
2. In large mixing bowl combine brown sugar, butter, eggs, and vanilla.
3. Beat at medium speed until creamy (1-2 minutes).
4. Reduce speed to low, add flour, baking powder, baking soda, and salt.
   Beat until well mixed.
5. By hand stir in white chocolate and macadamia nuts.
6. Drop dough by rounded tablespoonful 2 inches apart onto greased
   cookie sheet.
7. Bake 9-12 minutes or until lite golden brown. Cool one minute. Remove
   from cookie sheet.
