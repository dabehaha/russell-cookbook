Cedar Planked Salmon (v1.0)
===========================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: 35 minutes

Ingredients
-----------

-  3 (12 inch) untreated cedar planks
-  1/3 cup vegetable oil
-  1-1/2 teaspoon rice vinegar
-  1 teaspoon sesame oil
-  1/3 cup soy sauce
-  1/4 cup chopped green onion
-  1 tablespoon grated fresh ginger root
-  1 teaspoon minced garlic
-  2 (2 pound) salmon fillets, skin removed

Procedure
---------

1. Soak the cedar planks for at least 1 hour in warm water. Soak longer
   if you have time.
2. In a shallow dish, stir together the vegetable oil, rice vinegar,
   sesame oil, soy sauce, green onions, ginger, and garlic.
3. Place the salmon fillets in the marinade and turn to coat. Cover and
   marinate for at least 15 minutes, or up to one hour.
4. Preheat an outdoor grill for medium heat.
5. Place the planks on the grate. The boards are ready when they start
   to smoke and crackle just a little.
6. Place the salmon fillets onto the planks and discard the marinade.
7. Cover, and grill for about 20 minutes.
8. Fish is done when you can flake it with a fork. It will continue to
   cook after you remove it from the grill.
