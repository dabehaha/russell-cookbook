Tacos in Pasta Shells (v1.0)
============================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: 1 hour

Ingredients
-----------

-  1 1/4 pounds lean ground beef
-  1 (8 ounce) package cream cheese
-  3 tablespoons and 1 teaspoon of taco seasoning (or all of this `taco
   seasoning <./taco-seasoning.html>`__)
-  18 jumbo pasta shells
-  2 tablespoons butter, melted
-  1 cup taco sauce
-  1 cup shredded Cheddar cheese
-  1 cup shredded Monterey Jack cheese
-  1-1/2 cups crushed tortilla chips
-  1 cup sour cream

Procedure
---------

1. In a large skillet, brown beef over medium heat until no longer pink;
   drain.
2. Add cream cheese, taco seasoning; mix and simmer for 5 minutes.
3. Meanwhile, bring a large pot of lightly salted water to a boil. Add
   pasta and cook for 8 to 10 minutes or until al dente; drain. Toss
   cooked shells in butter.
4. Preheat oven to 350 degrees F (175 degrees C).
5. Fill shells with beef mixture and arrange in a 9x13 inch baking dish;
   pour taco sauce over shells. Cover with foil and bake in preheated
   oven for 15 minutes.
6. Remove dish from oven and top with Cheddar cheese, Monterey Jack
   cheese and tortilla chips; return dish to oven to cook for 15 minutes
   more. Top with sour cream and onions; serve.
