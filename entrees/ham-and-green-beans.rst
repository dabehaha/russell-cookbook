Ham and Green Beans (v1.0)
==========================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  Ham bone
-  1 large bag of fresh green beans
-  potatoes (new or russet)
-  2 onions
-  salt and pepper

Procedure
---------

1. Place ham, green beans, potatoes, and onions in large pan. Fill pan
   with water just covering the meat. Salt, pepper and garlic powder to
   taste.
2. Bring water to a boil and then simmer for about 5 hours.

Notes
-----

Can substitute green beans for white beans. White beans do need to be
soaked overnight.
