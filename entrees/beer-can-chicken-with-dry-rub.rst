Beer Can Chicken with Dry Rub (v1.0)
====================================

| Created By: Unknown
| Created On: Unknown
| Servings: 4
| Prep. Time: Unknown
| Cook Time: 1 hour 35 minutes

Ingredients
-----------

-  1 (4-pound) whole chicken
-  2 tablespoons vegetable oil
-  2 tablespoons salt
-  1 teaspoon black pepper
-  3 tablespoons of your favorite dry spice rub (try this
   `recipe <./entrees/dry-rub.md>`__)
-  1 can beer

Procedure
---------

1. Remove neck and giblets from chicken and discard.
2. Rinse chicken inside and out, and pat dry with paper towels.
3. Rub chicken lightly with oil then rub inside and out with salt,
   pepper and dry rub. Set aside.
4. Open beer can and take several gulps (make them big gulps so that the
   can is half full).
5. Place beer can on a solid surface. Grabbing a chicken leg in each
   hand, plunk the bird cavity over the beer can.
6. Transfer the bird-on-a-can to your grill and place in the center of
   the grate, balancing the bird on its 2 legs and the can like a
   tripod.
7. Cook the chicken over medium-high, indirect heat (i.e. no coals or
   burners on directly under the bird), with the grill cover on, for
   approximately 1-1/4 hours or until the internal temperature registers
   165 degrees F in the breast area and 180 degrees F in the thigh, or
   until the thigh juice runs clear when stabbed with a sharp knife.
8. Remove from grill and let rest for 10 minutes before carving.
