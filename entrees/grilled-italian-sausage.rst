Grilled Italian Sausage (v1.0)
==============================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

Italian Sausage
^^^^^^^^^^^^^^^

-  4 hot or sweet Italian sausage links
-  4 tablespoon olive oil
-  kosher salt and freshly ground black pepper
-  4 hoagie buns, split three- quarters of the way through
-  1/4 cup Dijon mustard

Grilled Red Pepper Relish
^^^^^^^^^^^^^^^^^^^^^^^^^

-  3 red bell peppers, grilled, peeled, seeded, and coarsely chopped
-  3 cloves roasted garlic, coarsely chopped
-  2 tablespoon red wine vinegar
-  3 tablespoon extra virgin olive oil
-  2 tablespoon finely freshly chopped fresh flat leaf parsley leaves
-  2 tablespoon finely chopped fresh basil leaves
-  kosher salt and freshly found black pepper

Onion Sauce
^^^^^^^^^^^

-  2 tablespoon canola oil
-  2 large onions, halved and cut 1/4 inch thick
-  1 teaspoon ancho chile powder
-  1/2 teaspoon ground cinnamon
-  1/4 cup ketchup
-  1 teaspoon hot sauce
-  1/2 teaspoon kosher salt
-  1/4 teaspoon freshly ground black pepper

Procedure
---------

1. Heat your grill medium-high

Grilled Red Pepper Relish
^^^^^^^^^^^^^^^^^^^^^^^^^

1. Combine the peppers, garlic, red wine vinegar, extra virgin olive
   oil, parsley, and basil in a medium bowl and season with salt and
   pepper.
2. Let the relish sit at room temperature for 30 minutes before serving.

Onion Sauce
^^^^^^^^^^^

1. Heat the oil in a medium saucepan over medium heat.
2. Add the onions, and cook, stirring occasionally, until soft, about 10
   minutes.
3. Stir in the chile powder and cinnamon and cook for 1 minute.
4. Add the ketchup, 1/2 cup water, the hot sauce, salt, and black
   pepper, and bring to a simmer.
5. Cook for 10 to 15 minutes or until thickened.
6. Transfer to a bowl and let cool to room temperature.

Italian Sausage
^^^^^^^^^^^^^^^

1. Brush the sausages with 2 tablespoons of the olive oil and season
   with salt and pepper.
2. Grill the sausages until golden brown, slightly charred on both
   sides, and cooked through, about 12 to 15 minutes.
3. Brush the inside of the buns with the remaining 2 tablespoons olive
   oil and season with salt and pepper.
4. Place on the grill, cut side down, and grill until lightly golden
   brown, about 1 minute.

Bringing It All Together
^^^^^^^^^^^^^^^^^^^^^^^^

1. Spread the bottom half of each bun with some of the mustard and top
   with a sausage and some of the onion sauce and grilled pepper relish.

Notes
-----

The onion sauce can be made 1 day in advance, covered, and refrigerated.
Bring to room temperature before serving.
