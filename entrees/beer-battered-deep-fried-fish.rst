Beer Battered Deep Fried Fish (v1.0)
====================================

| Created By: Unknown
| Created On: Unknown
| Servings: 4-6
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 - 2 1/2 lb. halibut fillets
-  1 c. flour
-  1/2 tsp. paprika
-  1/4 tsp. salt
-  1/8 tsp. pepper
-  3/4 c. beer
-  Oil for deep frying
-  Lemon wedges and tartar sauce.

Procedure
---------

1. Heat the oil in a deep fat fryer to 375 degrees.
2. Cut fillets in to 2 inch squares.
3. Make a batter by mixing dry ingredients and slowly stirring in the
   beer. Beat until smooth.
4. Dip each piece of fish into batter. Drain.
5. Deep fry a few pieces at a time until golden brown.
6. Drain and and place on paper towel lined serving platter.

Notes
-----

Serve with salt and lemon wedges or tartar sauce.
