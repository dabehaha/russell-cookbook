Dry Rub (v1.0)
==============

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1/4 cup paprika
-  2 teaspoons cayenne pepper
-  3 teaspoons freshly ground black pepper
-  3 tablespoons garlic powder
-  1 tablespoon and 1-1/2 teaspoons onion powder
-  3 tablespoons salt
-  1 tablespoon and 3/4 teaspoon dried oregano
-  1 tablespoon and 3/4 teaspoon dried thyme

Procedure
---------

1. In a small bowl mix together ingredients.
