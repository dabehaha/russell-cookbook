Ricotta Calzones (v1.0)
=======================

| Created By: Unknown
| Created On: Unknown
| Servings: 6 to 8 (makes 9 inch calzones)
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

Dough
^^^^^

-  4 cups (22 ounces) bread flour
-  2-1/4 teaspoons instant or rapid rise yeast
-  1-1/2 teaspoons salt
-  2 tablespoons extra virgin olive oil
-  1-1/2 cups plus 1 tablespoon water heated to 105 degrees

Filling
^^^^^^^

-  2 tablespoons olive oil
-  3 garlic cloves, minced
-  1/4 teaspoon red pepper flakes
-  16 ounces whole milk ricotta (2 cups)
-  8 ounces fresh mozzarella cheese, shredded (2 cups)
-  1 1/2 ounces grated parmesan cheese (3/4 cup)
-  1 large egg yolk
-  1 tablespoon fresh oregano
-  1/4 teaspoon salt
-  1/8 teaspoon pepper
-  extra virgin olive oil
-  kosher salt

Sauce for Dipping
^^^^^^^^^^^^^^^^^

-  1-28 ounce can crushed tomatoes
-  1 tablespoon extra virgin olive oil
-  1 teaspoon red wine vinegar
-  2 garlic cloves, minced
-  1 teaspoon salt
-  1 teaspoon dried oregano
-  1/4 teaspoon pepper

Procedure
---------

Dough
^^^^^

1. Using a stand mixer fitted with dough hook, combine flour, yeast, and
   salt on low speed.
2. Increase speed to medium-low, add olive oil, then gradually add
   water; continue to mix until mixture comes together and smooth,
   elastic dough forms, about 10 minutes.
3. Transfer dough to large, lightly greased bowl; cover with greased
   plastic wrap and let rise at room temperature until doubled in size,
   1-1/2 to 2 hours.

Sauce for Dipping
^^^^^^^^^^^^^^^^^

1. Mix all ingredients together and heat.

Filling
^^^^^^^

1. Heat olive oil, garlic, and pepper flakes in 8 inch skillet over
   medium heat until garlic is fragrant and begins to sizzle, 1-1/2 to 2
   minutes.
2. Transfer to small bowl and let cool, stirring occasionally, for 10
   minutes.
3. Combine ricotta, mozzarella, parmesan, egg yolk, oregano, salt,
   pepper, and cooked garlic oil in bowl; cover with plastic and
   refrigerate until needed.

Bringing It All together
^^^^^^^^^^^^^^^^^^^^^^^^

1.  Adjust oven rack to lowest position, set baking stone on rack, and
    heat oven to 500 degrees for at least 30 minutes.
2.  Line baking sheet with parchment paper and spray parchment lightly
    with vegetable oil spray.
3.  Transfer dough to clean counter. Divide dough in half, then cut each
    half into thirds.
4.  Loosely shape each piece of dough into a ball.
5.  Transfer to baking sheet and cover with greased plastic. Let dough
    rest for 15 to 30 minutes.
6.  Cut eight 9 inch square pieces of parchment.
7.  Roll 1 dough ball into 9 inch round (keep other pieces covered).
8.  Set round onto parchment square and cover with another parchment
    square; roll out another ball, set dough round on top of first, and
    cover with another parchment square.
9.  Repeat to form stack of 3 rounds, covering top round with parchment
    square. Form second stack of 3 with remaining dough balls and
    parchment squares.
10. Remove top parchment square from first stack of dough rounds and
    place rounds on with parchment beneath on work surface; if dough
    rounds have shrunk, gently and evenly roll out again to 9 inch
    rounds.
11. Spread scant 1/2 cup filling onto bottom half of each round, leaving
    1 inch border at bottom uncovered.
12. Fold top half of dough over bottom half, leaving 1/2 inch border of
    bottom layer uncovered, then press and crimp edges to seal.
13. With sharp knife, cut 5 slits, about 1-1/2 inches long, diagonally
    across the top of calzone, making sure to cut through only top layer
    of dough.
14. Brush tops and sides of calzones with olive oil and lightly sprinkle
    with kosher salt.
15. Trim excess parchment paper; slide parchment with calzones onto
    pizza peel, then slide onto baking stone, evenly spacing them apart.
16. Bake until golden drown, about 11 minutes.
17. While first batch of calzones bake, shape and top second batch.
18. Transfer calzones to wire rack and discard parchment; let cool for 5
    minutes. While first batch cools, bake second batch. Serve.
