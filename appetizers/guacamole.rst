Guacamole (v1.0)
================

| Created By: Unknown
| Created On: Unknown
| Servings: 4
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  3 avocados
-  1 lime, juiced
-  1 teaspoon salt
-  1/2 cup diced shallots
-  3 tablespoons chopped fresh cilantro
-  2 roma tomatoes, diced
-  1 teaspoon minced garlic
-  1 pinch of cayenne pepper (optional)

Procedure
---------

1. In a medium bowl, mash together the avocados, lime juice, and salt.
2. Mix in shallot, cilantro, tomatoes, and garlic.
3. Stir in cayenne pepper.
4. Refrigerate 1 hour for best flavor, or serve immediately.
