Short Rib Lasagna Rolls (v1.0)
==============================

| Created By: Unknown
| Created On: Unknown
| Servings: 4-6
| Prep. Time: Unknown
| Cook Time: 4 hours 15 minutes

Ingredients
-----------

Ribs
^^^^

-  2 tablespoons olive oil
-  2-1/2 pounds beef short ribs
-  2 teaspoons kosher salt, plus extra for seasoning
-  1 teaspoon freshly ground black pepper, plus extra for seasoning
-  1 onion, roughly chopped
-  4 cloves garlic, peeled and smashed
-  2 (4-inch) sprigs fresh rosemary
-  2 cups red wine, such as pinot noir
-  2 cups beef broth

Filling
^^^^^^^

-  3/4 cup milk
-  1/2 cup heavy cream
-  1-1/2 cups grated Pecorino Romano (6 ounces)
-  1 cup shredded mozzarella (4 ounces)
-  1 (10-ounce) packet frozen spinach, thawed, drained, and squeezed of
   excess liquid
-  1/4 cup chopped fresh basil
-  2 cloves garlic, minced
-  1 teaspoon kosher salt, plus extra for seasoning
-  1/2 teaspoon freshly ground black pepper, plus extra for seasoning
-  12 lasagna noodles (about 10 ounces)
-  Butter, for greasing baking dish
-  1 (25-ounce) jar marinara sauce
-  1/2 cup freshly grated Parmesan
-  Olive oil, for drizzling

Procedure
---------

Ribs
^^^^

1.  In a large Dutch oven or heavy-bottomed stockpot, heat the oil over
    medium-high heat.
2.  Season the ribs with 2 teaspoons salt and 1 teaspoon pepper.
3.  Add the ribs to the pan and cook until brown, about 4 minutes each
    side. Remove the ribs and set aside.
4.  Add the onions, garlic, and rosemary. Season with salt and pepper.
5.  Cook until the onions are translucent and soft, about 5 minutes.
6.  Increase the heat to high. Add the wine and scrape up the brown bits
    that cling to the bottom of the pan with a wooden spoon.
7.  Add the beef broth and ribs to the pan. Bring the mixture to a boil.
8.  Reduce the heat to a simmer, cover the pan, and cook until the meat
    is very tender, 2 1/2 to 3 hours.
9.  Remove the ribs and set aside until cool enough to handle, about 20
    minutes.
10. Discard the bones and cooking liquid. Using 2 forks, shred the meat
    into 2-inch long pieces (to yield approximately 2 1/4 cups shredded
    meat).

Filling
^^^^^^^

1. In a medium heavy-bottomed saucepan, bring the milk and cream to a
   simmer over medium heat.
2. Reduce the heat to low.
3. Add the cheeses and whisk until melted and the sauce is smooth.
4. Remove the pan from the heat and stir in the spinach, basil, and
   garlic.
5. Place the shredded meat in a medium bowl and pour the spinach mixture
   on top. Add the salt and pepper.
6. Using a fork, mix until combined. Taste and adjust the seasoning with
   salt and pepper, if needed.

Bringing It All Together
^^^^^^^^^^^^^^^^^^^^^^^^

1. Bring a large pot of salted water to a boil over high heat.
2. Add the pasta and cook until just tender but still firm to the bite,
   stirring occasionally, 8 to 10 minutes. Drain and set aside.
3. Place an oven rack in the center of the oven. Preheat the oven to 400
   degrees F. Butter a 9 by 13-inch glass baking dish.
4. Spread 1 cup marinara sauce in the bottom of the prepared baking
   dish.
5. Lay 4 noodles flat on a dry work surface. Spread 1/4 to 1/3 cup of
   the filling mixture evenly along each noodle. Roll up and place
   seam-side down in the baking dish. Repeat with the remaining noodles
   and filling to make 12 lasagna rolls.
6. Spoon the remaining marinara sauce on top and sprinkle with Parmesan.
   Drizzle with olive oil, and bake until the lasagna rolls are heated
   through and the cheese is beginning to brown, about 25 minutes.
