Chicken and Dumplings (v1.0)
============================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 QT chicken stock
-  3 1/2 lb whole chicken
-  2 cups self rising flour White Lily if possible
-  2 cups buttermilk
-  Salt and pepper

Procedure
---------

1.  Bring the stock to a simmer in a large stock pot over high heat.
2.  Add the chicken, keeping the stock at a gentle simmer.
3.  Cover and cook for one hour or until the meat is falling from the
    bone.
4.  Remove the chicken from the pot, keeping the stock at a simmer.
5.  Remove the skin and meat (in big chunks) from the chicken.
6.  Discard the skin and the bones. Reserve the meat and set aside.
7.  In a large mixing bowl, combine the flour and the buttermilk and
    season with black pepper.
8.  If the dough looks dry, add a few tablespoons of hot broth and a few
    additional tablespoons of buttermilk.
9.  Fold the dough lightly (it will look wet).
10. Season the broth with salt and pepper as needed.
11. Wet a spoon in the simmering broth, then scoop a heaping tablespoon
    of dumpling mixture and gently drop it into the broth over the
    largest simmering bubbles.
12. Working quickly, repeat with the rest of the batter.
13. Reduce to a low simmer, cover and cook for 10-15 minutes.
14. As the dumplings cook, baste as needed with broth. It's important
    that the dumplings don't overlap or sit on top of each other.
    Carefully push some of the dumplings to the side and add the
    chicken. Cover and simmer for another minute to heat through.
15. Check that the dumplings are cooked through by using a cake tester.
16. Place dumplings, chicken and broth in a bowl. Finish with black
    pepper.
