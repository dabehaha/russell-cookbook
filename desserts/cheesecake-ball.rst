Cheesecake Ball (v1.0)
======================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 (8oz) package cream cheese
-  1/2 cup butter
-  1/4 teaspoon vanilla
-  3/4 cup powdered sugar
-  2 tablespoon brown sugar
-  3/4 cup mini chocolate chips
-  3/4 finely chopped pecans (optional)
-  chocolate or regular graham crackers

Procedure
---------

1. In a large bowl, beat cream cheese, butter and vanilla until fluffy.
   Gradually add brown sugar and powdered sugar until combined.
2. Mix in chocolate chips with your hand and cover.
3. Refrigerate for 2 hours.
4. Remove from refrigerator and form mixture into a ball and place on a
   serving plate.
5. Gently place pecans on the cream cheese ball (or you can use more
   chocolate chips) and refrigerate for another hour.

Notes
-----

Serving
^^^^^^^

Serve with chocolate graham crackers, regular graham crackers, or fudge
stick cookies for a real treat.
