Biscuits and Gravy (v1.0)
=========================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 pound sausage
-  flour
-  1 quart half & half
-  1 teaspoon salt
-  1/2 teaspoon pepper
-  1 package of biscuits

Procedure
---------

1. Cook sausage.
2. Remove sausage from pan, leaving grease in bottom. Grease should
   cover the bottom of the pan. (If there is not enough grease, melt
   some butter)
3. Add flour to grease to create a rue.
4. When rue has formed slowly add half and half while mixing.
5. Let simmer until thick.
6. Return sausage to gravy.
7. Cook biscuits as package directs.
