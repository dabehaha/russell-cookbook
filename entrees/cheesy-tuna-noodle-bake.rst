Cheesy Tuna Noodle Bake (v1.0)
==============================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  3 cups cooked noodles, rinsed and drained
-  1 cup frozen peas
-  1 tablespoon dried onion flakes
-  1 (10 3/4 oz) can Campbell’s Healthy Request Cream of Mushroom Soup
-  1 (6oz) can tuna, packed in water (solid white Albacore best)
-  1 scant cup shredded reduced fat Kraft cheddar cheese
-  1 tablespoon dijon mustard
-  1/8 teaspoon black pepper

Procedure
---------

1. Preheat oven to 350 degrees.
2. Spray with butter flavored cooking spray an 8 by 8 inch backing dish
   and set aside.
3. In large bowl combine noodles, onion flakes and soup.
4. Add tuna, cheese, mustard and pepper.
5. Pour into 8 by 8 inch baking dish.
6. Cover and bake for 30 minutes.
7. Uncover and bake an additional 5 minutes.

Notes
-----

From Mable: 2 3/4 cups uncooked noodles usually makes 3 cups cooked
