French Toast (v1.0)
===================

| Created by: Chris A. Russell
| Created on: Unknown
| Servings: 2
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 - Eggs
-  1/2 cups milk
-  1 teasppon vanilla
-  3 tablespoon all purpose flour
-  1/8 teaspoon salt
-  3 tablespoons butter
-  6 slices thick sliced French bread
-  1 tablespoon powdered sugar
-  butter
-  syrup

Procedure
---------

1. Beat the eggs in a large shallow bowl.
2. Add the milk, vanilla, flour, and salt to the eggs. Beat the mixture
   with an electric mixer. Be sure all the flour is well combined
3. Heat a large skillet over medium heat. When the surface is hot, add a
   teaspoon of butter.
4. Dip the bread, a slice at a time, into the batter, being sure to coat
   each side well. Drop the bread into the hot pan (as many as will fit
   at one time) and cook for 2 to 3 minutes per side or yntil the
   surface is golden brown. Repeat with the remaining pieces of bread.
5. Sprinkle with powdered sugar. Serve with butter and syrup.
