Perfect Pot Roast (v1.0)
========================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: 4 hours 30 minutes

Ingredients
-----------

-  Salt and freshly ground black pepper
-  One 3- to 5-pound chuck roast
-  2 or 3 tablespoons olive oil
-  whole onions, peeled and halved
-  6 to 8 whole carrots, unpeeled, cut into 2-inch pieces
-  1 cup red wine, optional
-  3 cups beef broth
-  2 or 3 sprigs fresh rosemary
-  2 or 3 sprigs fresh thyme

Procedure
---------

1.  Preheat the oven to 275 degrees F.
2.  Generously salt and pepper the chuck roast.
3.  Heat the olive oil in large pot or Dutch oven over medium-high heat.
4.  Add the halved onions to the pot, browning them on both sides.
5.  Remove the onions to a plate.
6.  Throw the carrots into the same very hot pot and toss them around a
    bit until slightly browned, about a minute or so.
7.  Reserve the carrots with the onions.
8.  If needed, add a bit more olive oil to the very hot pot. Place the
    meat in the pot and sear it for about a minute on all sides until it
    is nice and brown all over. Remove the roast to a plate.
9.  With the burner still on high, use either red wine or beef broth
    (about 1 cup) to deglaze the pot, scraping the bottom with a whisk.
10. Place the roast back into the pot and add enough beef stock to cover
    the meat halfway.
11. Add in the onions and the carrots, along with the fresh herbs.
12. Put the lid on, then roast for 3 hours for a 3-pound roast. For a 4
    to 5-pound roast, plan on 4 hours. The roast is ready when it's
    fall-apart tender.
