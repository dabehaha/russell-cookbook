Funeral Potatos (v1.0)
=============================

| Created By: `tastesbetterfromscratch.com <https://tastesbetterfromscratch.com/funeral-potatoes/>`_
| Created On: Unknown
| Servings: 8
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  30oz frozen hashbrowns, diced or shreded will work, THAWED*
-  2 cups sour cream
-  10.5 oz cream of chicken soup
-  10 tablespoons butter, divided, melted
-  1 teaspoon salt
-  1/4 teaspoon freshly ground black pepper
-  1 teaspoon dried minced onion
-  2 cups shredded cheddar cheese
-  2 cups corn flakes cereal

\* Allow potatoes to thaw in your fridge overnight or spread them on a baking sheet and warm them in the oven at 200 degrees for about 20 minutes until thawed

Procedure
---------

1. Preheat oven to 350 degrees Fresh
2. Combine sour cream, cream of chicken soup, 6 tablespoons of melted butter,salt, pepper, and dried onion in a bowl. Mix well.
3. Add potatoes and shredded cheese and stir to combine.
4. Spoon mixture into a single layer in a 9x13" pan.
5. Add cornflakes to a large ziplock bag and crush gently with your hands or a rolling pin.
6. Add remaining 4 tablespoons of melted butter to the crushed cornflakes and combine well.
7. Sprinkle mixture over potatoes.
8. Bake uncovered at 350 degrees F for 40-50 minutes.

