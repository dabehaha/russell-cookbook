Twice-Baked Potatoes (v1.0)
===========================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: 1 hour 10 minutes

Ingredients
-----------

-  3 large baking potatoes
-  1 tablespoon olive oil
-  1-1/2 tablespoons milk
-  1-1/2 tablespoons butter
-  1 cup sour cream
-  5 ounces sharp Cheddar cheese, grated (about 1 1/4 cups)
-  1/2 tablespoon garlic salt
-  Salt and pepper
-  3 slices bacon, cooked and crumbled
-  1/4 cup finely chopped green onion

Procedure
---------

1.  Preheat the oven to 400 degrees F.
2.  Wash the potatoes and pierce them with a fork.
3.  Rub the potatoes with the olive oil and place them on a jellyroll
    pan or a large cookie sheet with a rim.
4.  Bake the potatoes for 45 minutes to an hour, or until done.
5.  Remove the potatoes from the oven and cut them in half lengthwise.
    Set aside.
6.  Reduce the temperature of the oven to 350 degrees F.
7.  When the potatoes are cool enough to handle, scoop out the potato
    flesh into a large electric mixing bowl.
8.  Add the milk, butter, sour cream, cheese, garlic salt, and salt and
    pepper to taste. Mix until creamy.
9.  Divide the mixture evenly and spoon it back into the potato shells.
10. Return the potatoes to the oven for 15 minutes. Remove from the oven
    and garnish with the bacon and green onion.
