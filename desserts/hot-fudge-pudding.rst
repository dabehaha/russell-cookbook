Hot Fudge Pudding (v1.0)
========================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 cup sifted Gold Medal flour
-  2 teaspoons baking powder
-  1/4 teaspoon salt
-  3/4 cup sugar
-  6 tablespoon cocoa
-  1/2 cup milk
-  2 tablespoon shortening, melted
-  1 cup chopped nuts
-  1 cup brown sugar
-  1-3/4 cups HOT water

Procedure
---------

1. Sift together into bowl flour, baking powder, salt, sugar, 2
   tablespoons cocoa.
2. Stir in milk and shortening.
3. Blend in chopped nuts.
4. Spread in 9 inch square pan.
5. Combine brown sugar and 4 tablespoons of cocoa. Sprinkle mixture over
   batter.
6. Pour HOT water over entire batter.
7. Bake at 350 degrees for 45 minutes.
8. During baking, cake mixture rises to top and chocolate sauce settles
   to bottom. Invert pan on plate. Drip sauce from pan over it.

Notes
-----

Serving
^^^^^^^

Serve warm with or without cool whip.

Mables Notes: Instead of cool whip, Mable used whipped cream in her
early days of marriage. This was one of her husband’s favorites.
