Mexican Chicken Salad (v1.0)
============================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1/2 cup Vegetable or Olive Oil, plus a little more for the onion
-  4 Cloves Garlic, peeled and halved
-  Fresh hot green chiles to taste (2 serranos or 1 large jalapeño),
   stemmed and quartered
-  1/2 cup of Fresh Lime Juice
-  3/4 cup of Cilantro, roughly chopped
-  Freshly Ground Black Pepper
-  Salt
-  4 Medium Sized Chicken Breast Halves, boneless and skinless ((about 1
   1/4 pounds total or 567g) )
-  1 Medium White Onion, cut into 1/2-inch (1 1/4cm) slices
-  2 Ripe Avocados
-  2 Romaine hearts, sliced crosswise at 1/2inch (1 1/4cm) thick
-  1/3 cup of Grated Mexican queso anejo or other garnishing cheese like
   Romano or Parmesan

Procedure
---------

1. Heat the oil in a small skillet over medium. Add the garlic and
   chiles, and cook until the garlic is soft and lightly browned, 2 to 3
   minutes. Cool.
2. In a blender, puree the lime juice, cilantro, and 1 teaspoon of salt
   and 1/2 teaspoon black pepper, and the garlic and chile oil. Marinate
   the chicken with 1/3 of the mixture for up to 1 hour.
3. Heat a grill pan or gas grill over medium to medium-high (or start a
   charcoal fire and let it burn until the coals are medium hot and
   covered with white ash). Lightly brush the onion slices with oil and
   season with salt. Grill the onion and the chicken until the chicken
   is cooked through and the onion is well-browned, 5 to 6 minutes per
   side.
4. Chop the grilled onion into small pieces and put it into a bowl. Pit
   and peel the avocados, scooping the flesh in with the onion. Add
   another third of the garlic marinade and coarsely mash everything
   together with a potato masher, large fork, or the back of a spoon.
   Taste and season with salt, usually about 1/2 teaspoon.
5. Put sliced romaine into a large bowl, dress with the remaining third
   of the marinade, and toss to combine.
6. To serve, divide between four dinner plates. Put a portion of the
   guacamole into the center of each plate. Cut each breast into cubes
   and arrange over the guacamole. Sprinkle with queso anejo (or its
   substitute) and drizzle with more dressing.
