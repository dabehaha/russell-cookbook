``name-of-recipe`` (``version-of-recipe start at 1.0``)
=======================================================

| Created By: ``name-of-creator``
| Created On: ``date-of-creation``
| Servings: ``serving-size-of-recipe``
| Prep. Time: ``time-for-marinades-and-vege-cuts``
| Cook Time: ``time-for-mixing-and-cooking-or-baking``

Ingredients
===========

-  ``bullet-list-of-ingredients``
-  ``another-bullet-for-ingredients``

Procedure
=========

1. ``step-by-step-procedure``
2. ``another-step``

Notes ``opt.``
==============

Serving ``opt.``
----------------

``Notes about how to serve the item.``

Storage ``opt.``
----------------

``Notes about storing the leftovers.``

Variations ``opt.``
-------------------

``Slight variations on the recipe.``
