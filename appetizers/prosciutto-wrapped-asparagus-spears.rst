Prosciutto-Wrapped Asparagus Spears (v1.0)
==========================================

| Created By: Unknown
| Created On: Unknown
| Servings: 30
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  30 medium thin asparagus stalks
-  4 oz peppered Boursin cheese, softened
-  1/4 pound thinly sliced prosciutto
-  1/4 cup honey mustard

Procedure
---------

1. Trim the asparagus stalks so that they are 5 inches long.
2. In a deep skillet bring 1-1/2 inches salted water to boil and cook
   asparagus until tender, about 2 minutes.
3. In a colander drain the asparagus and rinse with cold water.
4. Drain the asparagus well on paper towels.
5. In a bowl mash the Boursin with a fork until it is smooth.
6. Cut 1 slice of prosciutto lengthwise into 1 inch strips and spread
   each strip with about 1/2 teaspoon Boursin.
7. Spread about 1/4 teaspoon mustard over the Boursin and wrap each
   strip in a spiral around an asparagus spear, trimming any excess.
8. Make more hors d’oeuvres with the remaining prosciutto, Boursin,
   mustard and asparagus spears in the same manner.
