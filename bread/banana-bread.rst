Banana Bread (v1.0)
===================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2/3 cup sugar
-  1/3 cup shortening
-  2 eggs
-  3 tablespoons sour cream
-  1 cup mashed bananas
-  2 cups sifted Gold Medal flour
-  1 teaspoon baking powder
-  1/2 teaspoon baking soda
-  1/2 teaspoon salt
-  1/2 cup chopped nuts

Procedure
---------

1. Preheat oven to 350 degrees F.
2. Sift togehter and set aside flour, baking powder, baking soda, and
   salt.
3. Mix together thoroughly sugar, shortening, and eggs.
4. Stir in sourcream and mashed bananas
5. Stir in sifted ingredients.
6. Blend in chopped nuts.
7. Pour mixture into well greased 9x5x3 loaf pan.
8. Let stand for 20 minutes **before** baking.
9. Bake until done about 50-60 minutes
