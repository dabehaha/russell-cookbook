Mom's Roast Beef (v1.0)
===========================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: 3-4 hours

Ingredients
-----------

- Eye of Round Roast
- Garlic Cloves (varies depending on size of roast)
- Salt
- Pepper
- Onion Powder
- Garlic Salt
- Onion (or two depending on size of roast), peeled and cut in halves
- Criso
- 1/4 cup floar
- Kitchen Bouquet

Procedure
---------

1. Preheat the oven to 350 degrees
2. Cut garlic cloves lengthwise into about 3-4 pieces per clove.
3. Make slits in roast every 1 to 1-1/2 inches apart and insert clove
   slices, pushing them below the surface.
4. Season entire roast with salt, pepper, and onion powder
5. On stovetop, melt a couple of Tbsps. of Crisco in a roasting pan that
   has a lid (will be used later), until hot.
6. Brown roast well on all sides in hot Crisco and then put in roasting
   pan with fat side up (if there is fat).
7. Place onion halves in with the roast.
8. Put uncovered roast and onions in preheated oven.
9. After approximately 30-45 minutes, cover the roasting pan with the
   lid and continue to cook for another 30-45 minutes or until it
   reaches an internal temp of 145 degrees.
10. Remove roast from the oven and tent with foil until it cools.
11. When cool remove onions and discard.
12. Mix 1/4 cup flour with 2 cup water. Make sure no lumps of flour
    remain.
13. Put uncovered roasting pan on stovetop and add the flour/water
    mixture.
14. Once simmering, scrape bottom of pan to loosen up anything stuck to
    the bottom.
15. If adding Kitchen Bouquet add it now.
16. Season to taste with garlic salt, pepper, and onion powder.
17. With a meat slicer, slice roast as thinly as possible.
18. Add sliced beef to gravy in roaster and warm.

Notes
-----

It is very important to slice the roast beef as thin as possible!

Slicers, especially powerful ones, can toss roast beef up toward the
cabinets. Keep this in mind when deciding where to slice the roast.

The roast can be removed and tented (step 10 and 11) in/on a separate
container/plate.

If it is taking too long to cool (and you have guests coming),
you can keep the roasts untented or untent a bit early to help cool them
down.

Serving
^^^^^^^

Serve as sandwiches with Provel cheese.