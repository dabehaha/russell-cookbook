Pork Gyros (v1.0)
=================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1/4 cup olive oil
-  1/4 cup dry red wine
-  4 cloves garlic, chopped
-  1 tablespoon finely chopped fresh oregano leaves
-  2 pounds pork tenderloin
-  kosher salt and freshly ground black pepper
-  4 pita pockets
-  1-1/2 cups baby spinach
-  1/2 red onion, thinly sliced
-  `yogurt-tomato sauce <./yogurt-tomato-sauce.html>`__
-  1 plum tomato, seeded and diced

Procedure
---------

1. Whisk together the oil, wine, garlic, and oregano in a baking dish.
2. Add the pork and turn to coat.
3. Cover and marinate in the refrigerator for at least 1 hour and up to
   4 hours.
4. Heat your grill to high.
5. Remove the pork from the marinade and pat dry with paper towels and
   season with salt and pepper.
6. Grill until golden brown on all sides and cooked to an internal
   temperature of 150 degrees, 15 to 18 minutes.
7. Transfer to a cutting board, tent loosely with foil, and let rest for
   5 minutes before slicing into 1/4 inch thick slices.
8. While pork is resting, grill the pita pockets for about 10 seconds
   per side. Transfer to a cutting board and slice off the top inch of
   each pita.
9. Fill the pita with sliced pork, some spinach and onion, a few
   spoonfuls of yogurt-tomato sauce and a sprinkling of tomatoes.
