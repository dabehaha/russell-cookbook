Green Bean Casserole (v1.0)
===========================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: 40 minutes

Ingredients
-----------

-  1 can (10-1/2 oz.) Condensed Cream of Mushroom Soup
-  3/4 cup milk
-  1/8 tsp. pepper
-  2 cans (14.5oz each) Green Beans, drained
-  1-1/3 cups Crispy Fried Onions

Procedure
---------

1. MIX soup, milk and pepper in a 1 1/2 -qt. baking dish. Stir in beans
   and 2/3 cup Crispy Fried Onions.
2. BAKE at 350°F for 30 min. or until hot. Stir.
3. TOP with remaining 2/3 cup onions. Bake 5 min. until onions are
   golden.
