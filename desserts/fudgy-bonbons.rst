Fudgy Bonbons (v1.0)
====================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 (12 oz) pkg semi-sweet chocolate chips
-  1/4 cup butter
-  1 (14 oz) can sweetened condensed milk (NOT evaporated)
-  1/2 cup finely chopped nuts (optional)
-  1 teaspoon vanilla
-  60 milk chocolate candy kisses, unwrapped
-  2 oz white baking bar
-  1 teaspoon shortening
-  1 cup flour

Procedure
---------

1. Heat oven to 350 degrees.
2. In medium saucepan combine chocolate chips and butter cook and stir
   over low heat until chips are melted and smooth.
3. Add sweetened condensed milk mix well.
4. Lightly spoon flour into measuring cup, level off. In medium bowl,
   combine flour, nuts, chocolate mixture and vanilla. Mix well.
5. Shape 1 tablespoon (use measuring spoon) of dough around each kiss,
   covering completely.
6. Place 1 inch apart on ungreased cookie sheet.
7. Bake for 6-8 minutes. Cookies will be soft and appear shiny but
   become firm as they cool. DO NOT OVERBAKE. Remove from cookie sheet
   and cool.
8. In small saucepan, combine white baking bar and shortening. Cook over
   low heat until melted and smooth. Drizzle over cookies. I leave some
   plain ones. Store in tightly covered container.
