Peking Roast (v1.0)
===================

| Created By: Unknown
| Created On: Unknown
| Servings: 6-8
| Prep. Time: Unknown
| Cook Time: 24 hours 10 minutes

Ingredients
-----------

-  5 pound roast beef
-  garlic
-  1 onion
-  1 cup cider vinegar (or white vinegar)
-  2 cups brewed coffee
-  2 cups water
-  salt and pepper

Procedure
---------

1. With a sharp knife, cut slits all the way through the roast and
   insert slivers of onions and garlic.
2. Put the meat into a bowl and slowly pour the vinegar over it so that
   it runs down the sides into the slits.
3. Cover with plastic wrap and refrigerate for 24-48 hours.
4. When ready to cook, pour the vinegar off.
5. Place the meat in a heavy dutch oven and brown in oil until very dark
   on all sides.
6. Pour the 2 cups of brewed coffee over the meat and then add 2 cups of
   water.
7. Cover and cook slowly for approximately 6 hours on top of the stove.
8. You may need to add more water at some point, so check it once in a
   while and add only a small amount of water at a time.
9. Season well, but do not add the salt and pepper until about 20
   minutes before serving.
