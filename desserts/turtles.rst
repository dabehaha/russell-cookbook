Turtles (v1.0)
==============

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 cups pecan or walnut halves
-  36 caramels
-  3 tablespoons butter
-  1/2 teaspoon vanilla
-  2/3 cup semisweet or milk chocolate pieces
-  1 1/2 teaspoons shortening

Procedure
---------

1. Cover baking sheet with wax paper. Arrange 5 nut pieces into turtles,
   one inch apart.
2. Place caramels and butter in double boiler and heat until melted.
   Remove from heat and add vanilla.
3. Drop by teaspoons over nuts making sure caramel touches all pieces to
   hold together. Melt chocolate with shortening and spread over
   caramel. Allow to set.

Notes
-----

Mable likes the milk chocolate better.
