Broccoli and Rice (v1.0)
========================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 onion, diced
-  1 - 10oz chopped frozen broccoli
-  1 cup minute rice
-  1 can cream of mushroom
-  1/4 cup water
-  1/2 velveta cheese log
-  1/2 cup milk

Procedure
---------

1. Preheat oven to 350 degrees.
2. Prepare broccoli as instructions on box direct and drain.
3. Combine all ingredients.
4. Cover with aluminum foil and bake for 40 minutes. Stir occasionally.
