Chicken Flamingo (v1.0)
=======================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 oz frozen broccoli
-  2 oz freshly sliced mushrooms
-  Pinch of red pepper flakes
-  4 ounces white wine
-  4 ounces lemon juice
-  4 ounces butter
-  2 ounces seasoned bread crumbs
-  1 ounce olive oil
-  1/4 ounce prosciutto
-  1 ounce mixed cheeses (parmesan, provolone, mozzarella)
-  1/4 teaspoon fresh ground garlic
-  6 ounces chicken breast

Procedure
---------

1. Lightly coat the chicken breast with Italian bread crumbs.
2. Cook on open grill until plump and tender.
3. Top with cheese blend until melted then with the following sauce: In
   a saute pan, add wine, lemon, butter mix, prosciutto ,garlic, and red
   pepper over medium heat.
4. Bring to a boil and add broccoli and mushrooms.
5. Pour over the chicken breasts with a slotted spoon, then with a
   spatula. Do not overpower with sauce!! The sauce should enhance, not
   smother the chicken.

Notes
-----

When Chris A. Russell cooks this at home he decreases the amount of
lemon juice! When he makes it for 6 people he does 8 oz butter, 8 oz
white wine and 3 oz lemon juice.

Serving
^^^^^^^

Serve with a side of white or red pasta and a vegetable of your choice.
