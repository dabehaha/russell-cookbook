Breakfast
=================

.. toctree::
   :glob:
   :maxdepth: 1

   ./french-toast
   ./pancakes
   ./biscuits-and-gravy
