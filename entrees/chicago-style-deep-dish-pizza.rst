Chicago Style Deep Dish Pizza (v1.0)
====================================

| Created By: Unknown
| Created On: Unknown
| Servings: 6-8 (Makes two 9 inch pizzas)
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

Dough
^^^^^

-  3 cups (16.25 ounces) all purpose flour
-  1/2 cup (2.75 ounces) yellow cornmeal
-  2-1/4 teaspoons instant or rapid rise yeast
-  2 teaspoons sugar
-  1-1/2 teaspoons salt
-  1-1/4 cups water, room temperature
-  3 tablespoons unsalted butter, melted, plus 4 tablespoons, softened
-  1 teaspoon plus 4 tablespoons olive oil

Sauce
^^^^^

-  2 tablespoons unsalted butter
-  1/4 cup grated onion
-  1/4 teaspoon dried oregano
-  Salt and pepper
-  2 garlic cloves, minced
-  1 (28 ounce) can crushed tomatoes
-  1/4 teaspoon sugar
-  2 tablespoons chopped fresh basil
-  1 tablespoon extra virgin olive oil

Bringing It All Together
^^^^^^^^^^^^^^^^^^^^^^^^

-  1 pound mozzarella, shredded (4 cups)
-  1/4 cup grated parmesan cheese

Procedure
---------

Dough
^^^^^

1. Using stand mixer fitted with the dough hook, mix together flour,
   cornmeal, yeast, sugar, and salt on low speed until combined, about 1
   minute.
2. Add water and melted butter and mix until fluffy combined, 1 to 2
   minutes, scraping down bowl as needed.
3. Increase speed to medium and knead until dough is glossy and smooth
   and pulls away from sides of bowl, 4 to 5 minutes. (dough will only
   pull away from sides while mixer is on. When mixer is off, the dough
   will fall back to sides.)
4. Usings fingers coat large bowl with 1 teaspoon of olive oil, rubbing
   excess oil from fingers onto blade of rubber spatula.
5. Using oiled spatula, transfer dough to prepared bowl, turning once to
   oil top.
6. Cover bowl tightly with plastic wrap. Let dough rise at room
   temperature until nearly doubled in size, 45 minutes to 1 hour.

Sauce
^^^^^

1. Melt butter in medium saucepan over medium heat.
2. Add onion, oregano, and 1/2 teaspoon salt and cook, stirring
   occasionally, until onion is softened and lightly browned, about 5
   minutes.
3. Stir in garlic and cook until fragrant, about 30 seconds.
4. Stir in tomatoes and sugar, bring to simmer, and cook until sauce has
   reduced to 2 1/2 cups, 25 to 30 minutes.
5. Off heat, stir in basil and oil, then season with salt and pepper to
   taste.

Bringing It All Together
^^^^^^^^^^^^^^^^^^^^^^^^

1.  Adjust oven rack to lowest position and heat oven to 425 degrees.
2.  Using rubber spatula, turn dough onto dry clean counter and roll
    into 15 by 12 inch rectangle with short side facing you.
3.  Spread softened butter over surface of dough using offset spatula,
    leaving 1/2 inch border along edges.
4.  Starting at short end, roll dough into tight cylinder.
5.  With seam side down, flattened cylinder into 18 by 4 inch rectangle.
6.  Cut rectangle, in half crosswise.
7.  Working with 1 half at a time, fold dough into thirds like business
    letter, then pinch seams together to form a ball.
8.  Return dough balls to oiled bowl, cover tightly with plastic, and
    let rise in refrigerator until nearly doubled in size 40 to 50
    minutes.
9.  Coat two 9 inch round cake pans with 2 tablespoons olive oil each.
10. Transfer 1 dough ball to clean counter and roll into 13 inch disk
    about 1/4 inch thick.
11. Transfer dough round to cake pan by rolling dough loosely around
    rolling pin, then unrolling dough into pan.
12. Lightly press dough into pan, working it into corners and 1 inch up
    sides.
13. If dough resists stretching, let it relax 5 minutes before trying
    again.
14. Repeat with remaining dough ball.
15. For each pizza, sprinkle 2 cups of mozzarella evenly over surface of
    dough.
16. Spread 11/4 cups of tomato sauce over cheese and sprinkle 2
    tablespoons of Parmesan cheese over sauce for each pizza.
17. Bake until crust is golden brown, 20 to 30 minutes. 18.Remove pizzas
    from oven and let rest for 10 minutes before slicing and serving.

Notes
-----

This dough must be prepared in a stand mixer. Place a damp kitchen towel
under the mixer and watch it at all times during kneading to prevent it
from wobbling off the counter. Handle the dough with slightly oiled
hands to prevent sticking. Grate the onion on the large holes of a box
grater.
