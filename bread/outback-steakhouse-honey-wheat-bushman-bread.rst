Outback Steakhouse Honey Wheat Bushman Bread (v1.0)
===================================================

| Created By: Unknown
| Created On: Unknown
| Servings: 8 loaves of bread
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1-1/2 cups warm water
-  2 tablespoons softened butter
-  1/2 cup honey
-  2 cups bread flour
-  2 cups wheat flour
-  1 tablespoon cocoa
-  1 tablespoon granulated sugar
-  2 teaspoons instant coffee granules
-  1 teaspoon salt
-  2-1/4 teaspoons yeast
-  1 teaspoon caramel coloring
-  3 tablespoons cornmeal, for dusting

Procedure
---------

1. Place all of the ingredients in the bread machine in the order
   listed, and process on dough setting. The dough will be a little on
   the wet side and sticky but if it seems too wet, add more flour.
2. When dough is done, let it rise for 1 hour. 3.Remove from pan, punch
   down, and divide into 8 portions.
3. Form portions into tubular shaped loaves about 6-8 inches long, and 2
   inches wide.
4. Sprinkle the entire surface of the loaves with cornmeal, and place
   them on 2 cookie sheets.
5. Cover and let rise for 1 hour.
6. Bake at 350 degreees for 20-25 minutes.

Notes
-----

For caramel color substitute 75 drops RED, 45 drops BLUE, 30 drops
YELLOW & 1/4 cup water.

Serving
^^^^^^^

Serve warm with whipped butter.
