Toasted Walnut Filling (v1.0)
=============================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1/2 cup firmly packed brown sugar
-  6 tablespoons butter
-  2 teaspoons water
-  2 egg yolks
-  2/3 cup walnuts (TOASTED)

Procedure
---------

1. Combine all ingredients EXCEPT walnuts in double boiler. Cook until
   thickened (doesn’t take long). Add walnuts. Cool. Spread between cake
   layers.
