Baked French Onion Soup (v1.0)
==============================

| Created By: Unknown
| Created On: Unknown
| Servings: 10
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  3 tablespoons vegetable oil
-  6 medium white onions, sliced
-  8 cups beef broth
-  1 cup water
-  2-1/2 teaspoon salt
-  1/2 teaspoon garlic powder
-  1/4 teaspoon ground black pepper
-  5 plain hamburger buns
-  10 slices provolone cheese
-  10 teaspoons shredded parmesan cheese

Procedure
---------

1.  Add 3 tablespoons oil to a large pot or saucepan over medium/high
    heat.
2.  Add sliced onions and saute for 30 minutes until the onions begin to
    soften and start to become translucent. You don't want them to
    brown.
3.  Add the beef broth, water, salt, garlic powder, and black pepper to
    the pan and bring mixture to a boil.
4.  When soup begins to boil, reduce heat and simmer for 45 minutes.
5.  To make the croutons cut off the top half of each top of the
    hamburger bun so that the bread is the same thickness as the bottom
    half of each bun. Throw the tops away. Now you should have 10 round
    pieces of bread - 5 bottoms buns and 5 top buns with the tops cut
    off.
6.  Preheat oven to 325 degrees.
7.  Place the bread in the oven directly on the rack and bake for 15 to
    20 minutes or until each piece is golden brown and crispy. Let these
    croutons aside until you need them.
8.  When the soup is done, spoon about 1 cup into an oven safe bowl.
9.  Float a crouton on top of the soup, then place a slice of provolone
    cheese on top of the crouton. Sprinkle 1/2 teaspoon of shredded
    parmesan cheese over the provolone.
10. Place the bowl into your oven set to high broil. Broil the soup for
    5 to 6 minutes or until cheese is melted and starting to brown (you
    may need to broil longer if you are making more than one bowl at a
    time).
11. Sprinkle an additional 1/2 teaspoon of parmesan cheese over the top
    of the soup and serve.
12. Repeat the precess to prepare remaining servings.
