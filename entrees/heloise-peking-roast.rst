Heloise Peking Roast (v1.1)
===========================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: 24 hours
| Cook Time: 4-6 hours

Ingredients
-----------

- Any cut of beef about 3 to 5 lbs
- Garlic, cut into slivers
- 1 cup of vinegar
- Vegetable Oil
- 2 cups of Strong (brewed) Black Coffee
- 2 cups of beef stock

Procedure
---------

1. Cut slits completely through the meat.
2. Insert slivers of garlic down into the slits.
3. Pour vinegar over the meat and make sure it runs down into the little
   slits where the garlic has been placed.
4. Place the meat in the refrigerator and leave it for 24 to 48 hours.
5. When ready to cook, place meat in big heavy pot and brown in oil
   until nearly burned on all sides.
6. Pour coffee and beef stock over the meat and cover.
7. Simmer on the stove for four to six hours.
8. Season with salt and pepper 20 minutes before serving.
