Pancakes (v1.0)
===============

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

Dry Ingredients
^^^^^^^^^^^^^^^

-  1-1/2 cup cake flour
-  3 tablespoon sugar
-  1/2 teaspoon baking soda
-  1 teaspoon baking powder
-  3/4 teaspoon salt

Wet Ingredients
^^^^^^^^^^^^^^^

-  2 eggs beaten
-  1-1/2 cup buttermilk
-  3 tablespoon melted butter
-  1 teaspoon vanilla

Procedure
---------

1. Mix dry ingredients together.
2. Mix wet ingredients together.
3. Mix wet and dry together. Should be a bit lumpy.
