Carrot Cake (v1.0)
==================

| Created By: Unknown
| Created On: Unknown
| Servings: 1 (9-inch) cake
| Prep. Time: Unknown
| Cook Time: 2 hours 50 minutes

Ingredients
-----------

-  Unsalted butter, for the pan
-  12 ounces, approximately 2-1/2 cups, all-purpose flour, plus extra
   for pan
-  12 ounces grated carrots, medium grate, approximately 6 medium
-  1 teaspoon baking powder
-  1 teaspoon baking soda
-  1/4 teaspoon ground allspice
-  1/4 teaspoon ground cinnamon
-  1/4 teaspoon freshly ground nutmeg
-  1/4 teaspoon salt
-  10 ounces sugar, approximately 1-1/3 cups
-  2 ounces dark brown sugar, approximately 1/4 cup firmly packed
-  3 large eggs
-  6 ounces plain yogurt
-  6 ounces vegetable oil
-  Cream Cheese Frosting, recipe follows

Procedure
---------

1. Preheat oven to 350 degrees F.
2. Butter and flour a 9-inch round and 3-inch deep cake pan. Line the
   bottom with parchment paper. Set aside.
3. Put the carrots into a large mixing bowl and set aside.
4. Put the flour, baking powder, baking soda, spices, and salt in the
   bowl of a food processor and process for 5 seconds. Add this mixture
   to the carrots and toss until they are well-coated with the flour.
5. In the bowl of the food processor combine the sugar, brown sugar,
   eggs, and yogurt.
6. With the processor still running drizzle in the vegetable oil. Pour
   this mixture into the carrot mixture and stir until just combined.
   Pour into the prepared cake pan and bake on the middle rack of the
   oven for 45 minutes. Reduce the heat to 325 degrees F and bake for
   another 20 minutes or until the cake reaches 205 to 210 degrees F in
   the center.
7. Remove the pan from the oven and allow cake to cool 15 minutes in the
   pan. After 15 minutes, turn the cake out onto a rack and allow cake
   to cool completely. Frost with cream cheese frosting after cake has
   cooled completely.
