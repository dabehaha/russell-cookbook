Chocolate Icing for Cream Puffs (v1.0)
======================================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 tablespoons shortening
-  2 (2 oz) square chocolates
-  1 cup sifted confectioners sugar
-  2 tablespoons boiling water

Procedure
---------

1. Melt together shortening and chocolate squares.
2. Blend in confectioners sugar and boiling water.
3. Beat until smooth, but not stiff.
