Cream Puffs (v1.0)
==================

| Created By: Mable
| Created On: Unknown
| Servings: 8 large puffs / 12 eclairs / 18 (size of walnuts) petits
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 cup water
-  1/2 cup butter
-  1 cup sifted Gold Medal flour
-  4 eggs

Procedure
---------

1. Preheat oven to 400 degrees.
2. Heat water and butter to boiling point in saucepan.
3. Stir in flour. Stir constantly until mixture leaves the pan and forms
   a ball (about 1 minute). Remove from hear. Cool.
4. Beat in eggs, one at a time.
5. Beat mixture until smooth and velvety. Drop from spoon onto uncreased
   baking sheet.

   ::

       Large puffs makes 8
       Eclairs make 12
       Petits (size of walnuts) makes 18

6. Cook large puffs for 45-50 minutes, eclairs 45-50 minutes, and petits
   30 minutes.

Notes
-----

This recipe is just for the pastery. For the chocolate icing, see
`Chocolate Icing for Cream
Puffs <./desserts/chocolate-icing-for-cream-puffs.md>`__. For the
filling, see `Cream Puff Filling <./desserts/cream-puff-filling.md>`__.
