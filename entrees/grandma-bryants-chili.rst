Grandma Bryant's Chili (v1.0)
=============================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 lb ground beef
-  1 lb ground pork
-  2 medium onions
-  2-3 large red pepper
-  4 garlic cloves
-  3 jalapeños
-  1/4 cup chili powder
-  2 - 30.5 oz cans chili beans
-  1 - 28oz Italian tomatoes
-  1 bottle beer
-  1 - 6oz tomato paste
-  2 teaspoon salt
-  1-1/2 teaspoon cumin
-  1-1/2 teaspoon oregano
-  2 bay leaves
-  1/4 cup yellow cornmeal

Procedure
---------

1. Cook ground beef, ground pork, onion, red pepper, garlic, and
   jalapeño. Cook until meat is brown and onion is soft.
2. Add chili powder.
3. Add chili beans, tomatoes, beer, tomato paste, salt, cumin, oregano,
   and bay leaves.
4. Add yellow cornmeal.

Notes
-----

Serving
^^^^^^^

Serve with oyster crackers and cheese.
