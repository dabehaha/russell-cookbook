Seven Minute Frosting (v1.0)
============================

| Created By: Mable
| Created On: Unknown
| Servings: Covers 2 layer cakes
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 egg whites
-  1-1/2 cup sugar
-  1/4 teaspoon cream of tartar
-  1/3 cup water
-  1 teaspoon vanilla

Procedure
---------

1. Combine egg whites, sugar, cream of tartar, and water in top of
   double boiler.
2. With an electric mixer, beat on high speed 1 minute
3. Place over boiling water. Beat on high speed for 7 minutes.
4. Remove top of double boiler from heat.
5. Add vanilla. Beat 2 minutes longer on high speed.
6. Spread on completely cooled cake.
