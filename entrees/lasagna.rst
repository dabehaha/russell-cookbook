Lasagna (v1.0)
==============

| Created By: Unknown
| Created On: Unknown
| Servings: 8
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1-1/2 pounds lean ground beef
-  1 onion, chopped
-  2 cloves garlic, minced
-  1 tablespoon chopped fresh basil
-  1 teaspoon dried oregano
-  2 tablespoons brown sugar
-  1-1/2 teaspoons salt
-  1 (29 ounce) can diced tomatoes
-  2 (6 ounce) cans tomato paste
-  12 dry lasagna noodles
-  2 eggs, beaten
-  1 pint part-skim ricotta cheese
-  1/2 cup grated Parmesan cheese
-  2 tablespoons dried parsley
-  1 teaspoon salt
-  1 pound mozzarella cheese, shredded
-  2 tablespoons grated Parmesan cheese

Procedure
---------

1.  In a skillet over medium heat, brown ground beef, onion and garlic;
    drain fat.
2.  Mix in basil, oregano, brown sugar, 1-1/2 teaspoons salt, diced
    tomatoes and tomato paste.
3.  Simmer for 30 to 45 minutes, stirring occasionally.
4.  Preheat oven to 375 degrees F (190 degrees C).
5.  Bring a large pot of lightly salted water to a boil.
6.  Add lasagna noodles, and cook for 5 to 8 minutes, or until al dente;
    drain. Lay noodles flat on towels, and blot dry.
7.  In a medium bowl, mix together eggs, ricotta, Parmesan cheese,
    parsley and 1 teaspoon salt.
8.  Layer 1/3 of the lasagna noodles in the bottom of a 9x13 inch baking
    dish.
9.  Cover noodles with 1/2 ricotta mixture, 1/2 of the mozzarella cheese
    and 1/3 of the sauce. Repeat.
10. Top with remaining noodles and sauce.
11. Sprinkle additional Parmesan cheese over the top.
12. Bake in the preheated oven 30 minutes. Let stand 10 minutes before
    serving.

Notes
-----

I double the meat, use 3 LBs, 1 lb ground beef, 1 lb veal and 1 lb
Italian sausage. I soften the onion in oil, then add the garlic, let it
release a bit then add the meat. I work the meat pretty well, so, no
chunks at all. I use 2 large can crushed and 1 large can small diced
tomatoes, 4 small cans paste. Double the spices.
