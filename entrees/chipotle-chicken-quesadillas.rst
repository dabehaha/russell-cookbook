Chipotle Chicken Quesadillas (v1.0)
===================================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  3 tablespoons olive oil
-  1 large red onion, chopped (about 1 1/2 cups)
-  4 garlic cloves, minced
-  1/3 cup minced chipotle peppers in adobo sauce (you'll need about 6
   chilis)
-  4 vine-ripened tomatoes (about 11/4 pounds), seeded and diced
-  3 scallions, thinly sliced
-  2 tablespoons honey
-  2 1/2 cups cooked shredded chicken (white and/or dark meat)
-  1 teaspoon salt
-  1/2 cup chopped cilantro
-  6 10-inch diameter flour tortillas
-  4 cups shredded sharp cheddar cheese
-  Sour cream, for serving (optional)
-  Lime wedges, for serving (optional)

Procedure
---------

1.  In a large sauté pan, heat the olive oil over medium heat.
2.  Add the onion and sauté until soft, about 5 minutes.
3.  Add the garlic and cook 1 minute more.
4.  Stir in the chipotle peppers and tomatoes. Cook until most of the
    liquid from the tomatoes has evaporated and the mixture is
    thickened, stirring often, about 20 minutes.
5.  Stir in the scallions, honey, chicken, salt and cilantro. Keep warm.
6.  Heat a sauté pan over medium heat and spray lightly with nonstick
    cooking spray.
7.  Lay a tortilla in the pan and heat until hot to the touch but not
    crisp (air pockets may form). Flip the tortilla over, then sprinkle
    2/3 cup of the cheese evenly overtop, being careful to keep it off
    the pan.
8.  Spread a heaping 1/2 cup of the chicken mixture over half of the
    tortilla.
9.  When the cheese is mostly melted, fold the tortilla over to cover
    the filling and form a half-moon shape.
10. Cook until the tortilla is crisp and golden and the cheese is
    melted, adjusting the heat as necessary, a few minutes per side.
11. Repeat with the remaining tortillas. Let the quesadillas rest a few
    minutes to allow the filling to set, then cut into wedges.

Notes
-----

If you have a large griddle, use that to make the quesadillas so you can
make a few at a time. Also, be sure to serve your quesadillas hot off
the pan (or off the griddle), otherwise, they’ll get soggy and the
cheese will harden. If this does happen, you can pop them in a 350
degree oven to heat them up, but they won’t be as crisp and gooey as
they are fresh. The chicken mixture can be made up to 2 days ahead,
cooled, covered, and refrigerated.

Serving
^^^^^^^

Serve with sour cream and lime wedges, if desired.
