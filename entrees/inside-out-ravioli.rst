Inside-Out Ravioli (v1.0)
=========================

| Created By: Mable
| Created On: Unknown
| Servings: 8-10
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 pound ground beef
-  1 medium onion, chopped (1/2 cup)
-  1 clove garlic, minced
-  1 tablespoon plus 1/4 cup vegetable oil divided
-  1 (10oz) package frozen chopped spinach
-  1 (16oz) can spaghetti sauce with mushroom
-  1 (8oz) can tomato sauce
-  1 (6oz) can tomato paste
-  1/2 teaspoon salt
-  dash black pepper
-  1 (7oz) package (2 cups) shell or elbow macaroni, cooked and drained
-  1 cup shredded sharp American cheese (4oz)
-  1/2 cup soft bread crumbs
-  2 eggs, well beaten

Procedure
---------

1. Brown ground beef, garlic and onion in 1 tablespoon oil in large
   skillet. Drain excess fat, if desired.
2. Cook spinach according to package directions.
3. Drain spinach, reserving liquid; add water to spinach liquid to make
   1 cup. Set spinach aside.
4. Add 1 cup spinach liquid, spaghetti sauce, tomato sauce, tomato
   paste, salt and pepper to beef mixture; simmer 10 minutes.
5. Combine reserved spinach with cooked macaroni, cheese, bread crumbs,
   beaten eggs and remaining 1/4 cup oil.
6. Spread spinach mixture in 13 by 9 by 2 inch daking dish.
7. Top with meat sauce. Bake uncovered, in a preheated 350 degree oven
   for 30 minutes.
8. Let stand 10 minutes before cutting into squares to serve.

Notes
-----

This can be made early in the day and refrigerated until baking time;
bring to room temperature before baking. Also can be frozen; if frozen,
let thaw, and bake for 45 minutes.
