Angel Chicken Pasta (v1.0)
==========================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: 90 minutes

Ingredients
-----------

-  6 skinless, boneless chicken breasts halves
-  1 stick butter
-  2 packs dry Italian salad dressing mix
-  1 cup white wine
-  2 cans golden mushroom soup
-  8 ounces cream cheese with chives
-  1 pound angel hair pasta

Procedure
---------

1. Preheat oven to 325 degrees.
2. In a large saucepan, melt butter over low heat.
3. Stir in the package of dressing mix.
4. Blend in wine and golden mushroom soup. Mix in cream cheese and stir
   until smooth. Heat through but do not boil.
5. Arrange chicken breasts in a single layer in a 9x13 inch baking dish.
   Pour sauce over.
6. Bake for 60 minutes in preheated oven.
7. Twenty minutes before the chicken is done bring a pot of lightly
   salted water to a rolling boil.
8. Cook pasta until al dente, about 5 minutes. Drain.
9. Serve chicken and sauce over pasta.
