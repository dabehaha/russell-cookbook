Baked Catfish (v1.0)
====================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  vegetable oil spray
-  lemon wedges (optional)
-  2 pounds catfish fillets (6 pieces)
-  3/4 cup low fat buttermilk
-  1/4 teaspoon salt
-  1/4 teaspoon hot pepper sauce
-  30 whole wheat crackers (wheatsworth crackers)
-  2 tablespoons margarine (melted)
-  2 tablespoons fresh parsley (optional)

Procedure
---------

1. Preheat oven to 400 degrees.
2. Lightly spray baking dish.
3. Combine buttermilk, salt and hot pepper sauce in shallow dish.
4. Place cracker crumbs on a plate.
5. Dip fillets first in buttermilk, then in crumbs. Coat evenly.
6. Place fillets in baking dish.
7. Drizzle 1 teaspoon margarine over each fillet.
8. Bake uncovered 15-20 minutes, or until fish flakes with a fork.
9. Sprinkle with parsley.

Notes
-----

From Mable: These are really good - No fish smell or fishy taste!
