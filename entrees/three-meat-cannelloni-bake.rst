Three Meat Cannelloni Bake (v1.0)
=================================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 cup minced onion
-  1/2 cup minced celery
-  1/3 cup minced carrot
-  2 tablespoons olive oil
-  2 cloves garlic, minced
-  12 ounces ground veal
-  12 ounces ground pork
-  12 ounces lean ground beef
-  1/2 cup white wine
-  1 cup beef broth
-  2 teaspoons chopped fresh rosemary
-  1 teaspoon Italian seasoning
-  1 bay leaf
-  salt to taste
-  ground black pepper to taste
-  2 egg yolks
-  2 tablespoons butter
-  2 tablespoons all-purpose flour
-  1 cup milk
-  1/4 teaspoon freshly ground nutmeg
-  3/4 cup grated Parmesan cheese
-  1/2 cup chopped parsley
-  4 cups tomato sauce
-  1/2 cup heavy whipping cream
-  1 pound fresh pasta sheets

Procedure
---------

1.  Heat the olive oil in a deep skillet. Add onion, celery, and carrot,
    and cook over moderate heat until softened.
2.  Add the garlic, and cook 1 minute.
3.  Add veal, pork, and beef. Cook, stirring occasionally, until meat is
    no longer pink.
4.  Add wine, and reduce for 1 minute.
5.  Stir in broth. Add herbs, bay leaf, and salt and pepper.
6.  Bring the mixture to a boil.
7.  Reduce heat, cover, and simmer for 15 minutes.
8.  Uncover, and reduce until almost dry. Discard bay leaf. Set aside to
    cool.
9.  Meanwhile, melt the butter or margarine in a saucepan set over
    moderately low heat.
10. Whisk in flour for approximately 2 minutes.
11. Whisk in milk, grated nutmeg, and salt and pepper.
12. Simmer, stirring occasionally, for 5 minutes or until thickened.
13. Stir in parsley and 1/2 cup Parmesan cheese.
14. Transfer the cooled meat mixture to a large bowl.
15. Mix in egg yolks. Mix in the cheese and parsley sauce. Set aside.
16. Combine the tomato sauce and cream, set aside.
17. Cut the pasta sheets crosswise into 5 inch lengths.
18. In a pot of boiling salted water, cook the noodles a few at a time
    until al dente. This should take only a minute or two with fresh
    pasta.
19. Transfer to a bowl of cold water. Spread the noodles in one layer on
    paper towels to drain.
20. Spoon 1/4 cup of the filling down the center of one noodle, and roll
    the noodle to enclose the filing.
21. Transfer the cannelloni, seam side down, to a well buttered gratin
    dish. Repeat with the remaining noodles and filling, arranging in
    single layer.
22. Ladle the tomato sauce over the cannelloni, and sprinkle with the
    remaining Parmesan cheese.
23. Bake at 400 degrees F (205 degrees C) for 10 minutes, or until
    bubbling.
24. Run under the broiler about 4 inches from the heat for 2 minutes, or
    until golden.
