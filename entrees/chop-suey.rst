Chop Suey (v1.0)
================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 pounds stew meat (trimmed and sliced into pieces)
-  2 large cans chop suey vegetables (undrained)
-  1 - 14 oz can bean sprouts (drained)
-  1 - 14 oz can water chestnuts (drained)
-  Soy sauce to taste

Procedure
---------

1. Brown the stew meat with some vegetable oil.
2. Season with salt, pepper, garlic powder and onion powder.
3. When meat is done cooking add some water, cover and simmer until
   tender. About 1-3 hours.
4. Transfer the meat and liquid to a pot.
5. Add vegetables, sprouts and chestnuts.
6. Add soy sauce to taste.
7. Cook until vegetables are warm.
