Potato Soup (v1.0)
==================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  6 cups peeled cubed red potatoes
-  2 cups water
-  1 cup sliced celery
-  1 cup thinly sliced carrots
-  1/2 cup finely chopped onions
-  2 teaspoons dried parsley flakes
-  2 chicken flavored bouillon cubes
-  1 teaspoon salt
-  1/8 teaspoon pepper
-  3 cups of milk
-  1/4 cup of flour
-  3/4 cup cubed white cheese

Procedure
---------

1. Combine first 9 incredients (red potatoes, water, celery, carrots,
   onions, parsley, bouillon cubes, salt, and pepper) in dutch oven.
2. Bring the contents to a boil.
3. Cover and reduce to a simmer for about 8 to 10 minutes or until
   vegetables are tender.
4. In a separate bowl, gradually stir in 1/4 cup of milk into flour to
   make a paste.
5. Add paste to soup.
6. Add remaining milk and cheese.
7. Cook over medium heat until milk has thickend.

Notes
-----

Mable recommends using Monetary Jack cheese for white cheese
