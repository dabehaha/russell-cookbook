*Please use this template if you are recommending a new recipe to the cookbook. Corrections and errors with existing recipes should use a different template*

# How do I fill out this template

Any section that looks `like this` should be deleted and replaced with the correct information. For example, `name-of-recipe` should be Chicken Noodle Soup **not** `Chicken Noodle Soup`. Any section with `opt.` in the header can be removed if not needed. Anything not in *Grave Accents* "\`" should be left alone beside adding more bullets or procedure steps. You may section the procedure or ingredients as needed to indicate different components of the same recipe (ex. dough and filling for a pie).

# Purposed Recipe
## `name-of-recipe` (v1.0)

Created By: `name-of-creator`  
Created On: `date-of-creation`  
Servings: `serving-size-of-recipe`  
Prep. Time: `time-for-marinades-and-vege-cuts`  
Cook Time: `time-for-mixing-and-cooking-or-baking`  

## Ingredients

- `bullet-list-of-ingredients`
- `another-bullet-for-ingredients`
- `add more bullets if needed`

## Procedure

1. `step-by-step-procedure`
2. `another-step`
3. `add another step if needed`

## Notes `opt.`

### Serving `opt.`

`Notes about how to serve the item.`

### Storage `opt.`

`Notes about storing the leftovers.`

### Variations `opt.`

`Slight variations on the recipe.`


