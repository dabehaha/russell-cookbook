White Chicken Chili (v1.0)
==========================

| Created By: Unknown
| Created On: Unknown
| Servings: 4
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 tablespoon vegetable oil
-  1 onion, chopped
-  3 cloves garlic, crushed
-  1 (4 ounce) can diced jalapeno peppers
-  1 (4 ounce) can chopped green chile peppers
-  2 teaspoons ground cumin
-  1 teaspoon dried oregano
-  1 teaspoon ground cayenne pepper
-  2 (14.5 ounce) cans chicken broth
-  3 cups chopped cooked chicken breast
-  3 (15 ounce) cans white beans
-  1 cup shredded Monterey Jack cheese

Procedure
---------

1. Heat the oil in a large saucepan over medium-low heat. Slowly cook
   and stir the onion until tender.
2. Mix in the garlic, jalapeno, green chile peppers, cumin, oregano and
   cayenne. Continue to cook and stir the mixture until tender, about 3
   minutes.
3. Mix in the chicken broth, chicken and white beans. Simmer 15 minutes,
   stirring occasionally.
4. Remove the mixture from heat.
5. Slowly stir in the cheese until melted. Serve warm.

Notes
-----

"Delicious white bean chili. Cha Cha says: 'It's kinda spicy, so watch
out!' Substitute mild green chiles for the jalapenos if you're scared!
Use more chicken and cheese as desired." — Cathy
