Quick Fried Shrimp with Sweet Toasty Garlic (v1.0)
==================================================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  3/4 cup peeled garlic cloves (about 2 large heads)
-  1 cup good quality oil, preferably extra virgin olive oil
-  Salt
-  Juice of 1 lime
-  2 canned chipotle chiles en adobo, seeded and cut into thin strips
-  2 limes, cut into wedges
-  2 pounds (about 48) medium- large shrimp, peeled (leaving the last
   joint and tail intact if you wish)
-  3 tablespoons chopped fresh cilantro or flat leaf parsley (optional)

Procedure
---------

Mojo De Ajo
^^^^^^^^^^^

1. Either chop the garlic with a sharp knife into 1/8 inch bits or drop
   cloves through the feed tube of a food processor with the motor
   running and process until the pieces are roughly 1/8 inch in size.
   You should have about 1/2 cup chopped garlic.
2. Scoop into a small (1 quart) saucepan, measure in the oil (you will
   need it all for even cooking) and 1/2 teaspoon salt and set over
   medium-low heat.
3. Stir occasionally as the mixture comes barely to a simmer (there
   should be just a hint of movement on the surface of the oil).
4. Adjust the heat to the very lowest possible setting to keep the
   mixture at that very gentle simmer (bubbles will rise in the pot like
   sparkling mineral water) and cook, stirring occasionally until the
   garlic is a soft and pale golden (the color of light brown sugar)
   about 30 minutes. The slower the cooking the sweeter the garlic.
5. Add lime juice to the pan and simmer until most of the juice has
   evaporated or been absorbed into the garlic, about 5 minutes.
6. Stir in the chiles, then taste the mojo de ajo and add a little more
   salt if you think it needs it.
7. Keep the pan over low heat, so the garlic will be warm when the
   shrimp are ready.
8. Scoop the lime wedges into a serving bowl and set on the table.

Shrimp
^^^^^^

1. Devein the shrimp if you wish: one by one lay the shrimp on your work
   surface, make a shallow incision down the back and pull or scrape out
   the dark (usually) intestinal tract.
2. Set a large (12 inch) heavy skillet (preferably nonstick) over
   medium-high heat and spoon in 1-1/2 tablespoons of the oil (but not
   any garlic) from the mojo.
3. Add half of the shrimp to the skillet, sprinkle generously with salt
   and stir gently and continuously until the shrimp are just cooked
   through, 3 to 4 minutes.
4. Stir in half the cilantro or parsley, if you’re using it. Scoop the
   shrimp onto a deep serving platter.
5. Repeat with another 1-1/2 tablespoons of garlicky oil and the
   remaining shrimp.
6. When all the shrimp are cooked, use a slotted spoon to scoop out the
   warm bits of garlic and chiles from the mojo pan, and scatter them
   over the shrimp.
7. You may have as much as 1/3 cup of the oil left over, for which you
   will be grateful (it’s wonderful for sautéing practically anything.).
8. If you’re a garlic lover, you’re about to have the treat of your
   life!

Notes
-----

The mojo de ajo keeps for a couple weeks in the refrigerator (the oil
will become solid but will liquify again at room temperature), so I
never recommend making a small amount. Mojo in the refrigerator
represents great potential for a quick wonderful meal. Warm cold mojo
slowly before using. For the best texture, cook the shrimp immediately
before serving. Or cook them an hour or so ahead, douse them with the
garlic mojo and serve it all at room temperature.

Serving
^^^^^^^

Serve with lime wedges to add sparkle.
