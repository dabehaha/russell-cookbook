Mom's Vegetable Soup (v1.0)
===========================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  5 cans 14.5 oz diced tomatoes with basil, garlic and oregano
-  1 can tomato sauce
-  bone marrow with meat (soup meat)
-  1 hand full of chopped fresh parsley
-  1 large bag of carrots
-  1 bag of celery
-  1 large onion
-  1 bag of egg noodles
-  salt
-  pepper
-  garlic powder

Procedure
---------

1. Cover meat with tomatoes and water. (Add enough water that meat is
   covered after placing tomatoes in pot.)
2. Add salt, pepper, garlic powder, and parsley. Add carrots, celery,
   and onion.
3. Bring pot to a boil and then cook on low.
4. Add egg noodles before noodles and cook until noodles are al dente.

Notes
-----

Be careful when adding noodles.

Serving
^^^^^^^

Consider serving with vienna bread.
