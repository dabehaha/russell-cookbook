Russell-Cookbook
================

The following repository was created to maintain the Russell Family
cookbook. It is here to capture every change to the recipes overtime and
(hopefully) prevent arguments about how to make Grandpa's stuffing.

This repository is not meant to replace family cookbooks. As many will
know using your phone or tablet in the kitchen can be difficult.
Instead, this repository was created as a means to preserve family
recipes, have a forum to recommend changes, and to exchange recipes.

==================================================

Contents:

.. toctree::
   :glob:
   :maxdepth: 1

   cooking-tips/README
   breakfast/README
   bread/README
   appetizers/README
   sides/README
   entrees/README
   desserts/README

How to ues the Cookbook
=========================

View
~~~~

To view all the contents of the cookbook, use the links above.
The cookbook is broken into the same categories as the
physical copy of the family cookbook with the addition of Cooking Tips.

Download
~~~~~~~~

You can download all the recipes in pdf form. Currently, this is broken.
This will be investigated in the future.

Recommend a Change/Addition
~~~~~~~~~~~~~~~~~~~~~~~~~~~

To recommend a change or addition to the repository, please use a GitLab
issue. Please follow this `link <https://gitlab.com/dabehaha/russell-cookbook/issues>`_ to create an issue.

When you create your issue, use and follow the templates provided.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
