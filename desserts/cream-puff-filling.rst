Cream Puff Filling (v1.0)
=========================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  Jell-O vanilla and pie filling (**not** instant)

Procedure
---------

1. Make the filling as directed on the box.
2. When you make the pudding cool it, cover it with plastic wrap and put
   it in the refrigerator. Plastic wrap prevents the pudding from
   getting a film on it.

Notes
-----

The recipe above was interperted from the following instructions given
by Mable.

::

    Rich custard takes 4 eggs and cream therefore I frequently use Jello vanilla and pie filling (NOT instant).
    The flavor is real good.  When you make the pudding cool it, cover it with plastic wrap and put in refrigerator.
    Plastic wrap prevents the pudding from getting a film on it. 

    Notes from Mable: When I was a kid cream puffs (filled with whipped cream) were special treats especially in winter (New Years).
    During the depression you can imagine the treat when butter, eggs and whipped cream were the ingredients of the day.
    (My mom cooled the puff mixture by putting the saucepan in a snow bank!)

    Back in the late 30s and early 40s when I lived in Milwaukee my girl friends and I would go once a week to a department store
    (like Famous) for a Hot Fudge Eclair! They were Huge! 

    Fill eclair with ice cream and top with hot fudge! Yum yum!
