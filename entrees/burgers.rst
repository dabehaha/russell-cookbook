Burgers (v1.0)
==============

| Created By: Unknown
| Created On: Unknown
| Servings: 4
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 pounds of beef
-  salt and pepper

Procedure
---------

1. Form burgers into 6-8 oz patties.
2. Salt and pepper.
3. Cook on hot grill for about 5-6 minutes per side or until cooked.
