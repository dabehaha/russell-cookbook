Chipotle Pork Tenderloin with Green Apple Juice Sauce (v1.0)
============================================================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2-1/2 pounds pork tenderloin
-  1/2 cup pureed canned chipotles
-  1 cup `Green Apple Juice
   Sauce <./green-apple-juice-sauce.html>`__

Procedure
---------

1. Rub the tenderloins with pureed chipotles, cover and refrigerate for
   at least 2 hours or overnight.
2. Prepare a wood or charcoal fire and let it burn down to embers, or
   preheat the broiler.
3. Grill the tenderloins for 10 to 12 minutes, turning. Slice very thin
   and pour the sauce over the meat.
