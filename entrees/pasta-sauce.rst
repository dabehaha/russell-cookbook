Pasta Sauce (v1.0)
==================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 cloves minced garlic
-  1 tablespoon olive oil
-  1 (29 oz) can crushed tomatoes
-  1 1/2 tablespoon brown sugar
-  1 teaspoon oregano
-  1 teaspoon salt
-  1 tablespoon chopped fresh basil

Procedure
---------

1. Heat olive oil in pan.
2. Add garlic and cook until fragrant.
3. Add tomatoes, brown sugar, oregano, and salt.
4. Simmer until thickened.
5. Before serving add basil.
