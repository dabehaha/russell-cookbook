Cream Cheese Frosting (v1.0)
============================

| Created By: Unknown
| Created On: Unknown
| Servings: 2 cups
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  8 ounces cream cheese
-  2 ounces unsalted butter, room temperature
-  1 teaspoon vanilla extract
-  9 ounces powdered sugar, sifted, approximately 2 cups

Procedure
---------

1. In the bowl of a stand mixer with paddle attachment, combine the
   cream cheese and butter on medium just until blended.
2. Add the vanilla and beat until combined.
3. With the speed on low, add the powdered sugar in 4 batches and beat
   until smooth between each addition.
4. Place the frosting in the refrigerator for 5 to 10 minutes before
   using.
