Spinach and Feta Stuffed Chicken Breast (v1.0)
==============================================

| Created By: Unknown
| Created On: Unknown
| Servings: 4
| Prep. Time: Unknown
| Cook Time: 45 minutes

Ingredients
-----------

-  4 chicken breasts
-  1 tablespoon instant minced garlic
-  2 tablespoons olive oil
-  1 (16 ounce) bag spinach (frozen works great)
-  1 cup feta cheese
-  2 cups seasoned bread crumbs
-  1/2 cup butter (melted)

Procedure
---------

1. Preheat oven to 350.
2. Prepare chicken for stuffing.(You can either, slice 2 thick chicken
   breasts in half and make 4, or beat 4 chicken breast until thin and
   wide.).
3. Cook spinach in garlic and oil on top of stove until done.
4. Add in feta cheese and mix well. (You can add as much as you like. I
   LOVE feta cheese!).
5. Distribute spinach mixture onto each chicken breast. (You may have
   some leftover).
6. Wrap chicken around mixture (kinda like a taco) and secure with a
   toothpick.
7. Roll each breast in bread crumbs until well coated.
8. Place in glass baking dish and pour butter over them.
9. Cook for 30 minutes and serve.

Notes
-----

Use fresh spinach, figure 1 lb. per 3 pieces of chicken. Mince the
garlic, at least 3-4 cloves, put in the oil first, let it simmer a bit,
then add the spinach. Once the spinach is completely wilted, remove the
pan from heat, then thoroughly mix the feta in. Don’t be afraid of the
feta. I used 1+ cup for folks who don't like it and they loved this. We
like to add chopped tomatoes to the mixture as well. I didn't wrap the
chicken over itself and trimmed a bit to try and get a better roll. Use
all the butter thoroughly covering the bread crumbs and you won't miss
the frying part. Drain excess butter from the glass baking dish before
it goes in oven to make the cleanup a bit easier.
