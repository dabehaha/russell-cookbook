Fluffy Dinner Rolls (v1.0)
==========================

| Created By: Unknown
| Created On: Unknown
| Servings: 11 rolls
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

Flour Paste
^^^^^^^^^^^

-  1/2 cup water
-  3 tablespoon bread flour

Dough
^^^^^

-  1/2 cup cold milk
-  1 large egg
-  2 cups (11 oz) bread flour
-  1-1/2 teaspoons instant or rapid rise yeast
-  2 tablespoons sugar
-  1 teaspoon salt
-  4 tablespoons unsalted butter, softened, plus 1/2 tablespoon melted

Procedure
---------

1. **FOR THE FLOUR PASTE**: Whisk water and flour together until no
   lumps remain. Microwave, whisking every 20 seconds, until mixture
   thickens to stiff, smooth and pudding-like consistency that forms
   mound when dropped from the end of whisk into bowl, 40 to 80 seconds.
2. **FOR THE DOUGH**: In bowl of stand mixer, whisk flour paste and milk
   together until smooth. Add egg and whisk until incorporated. Add
   flour and yeast. Fit stand mixer with dough hook and mix on low speed
   until all flour is moistened, 1 to 2 minutes. Let stand for 15
   minutes.
3. Add sugar and salt and mix on medium- low speed for 5 minutes. With
   mixer running, add softened butter, 1 tablespoon at a time. Continue
   to mix on medium low speed 5 minutes longer scraping down dough hook
   and sides of bowl occasionally (dough will stick to bottom of bowl).
4. Transfer dough to very lightly floured counter Knead briefly to form
   ball, seam side down, to lightly greased bowl, lightly coat surface
   of dough with vegetable oil spray and cover with plastic wrap. Let
   rise until doubled in volume, about 1 hour.
5. Grease 9 inch round cake pan and set aside. Transfer dough to
   counter. Press dough gently but firmly to expel all air. Pat and
   stretch dough to form 8 by 9 inch rectangle with short side facing
   you. Cut dough lengthwise into 4 equal strips and cut each strip
   crosswise into 3 equal pieces. Working with 1 piece at a time,
   stretch and press dough gently to form 8 by 2 inch strip. Starting on
   short side, roll dough to form snug cylinder and arrange shipped
   rolls seam side down in prepared pan, placing 10 rolls around edge of
   pan, pointing inward, and remaining 2 rolls in center. Cover with
   plastic and let rise until doubled, 45 minutes to 1 hour.
6. When rolls are nearly doubled, adjust oven rack to lowest position
   and heat oven to 375 degrees. Bake rolls until deep golden brown 25
   to 30 minutes. Let rolls cool in pan on wire rack for 3 minutes;
   invert rolls on rack, then reinvert. Brush tops and sides pos rolls
   with melted butter. Let rolls cool for at least 20 minutes before
   serving.

Notes
-----

We recommend weighing the flour for the dough. The slightest tackiness
of the dough aids in flattening and stretching it in step 5, so do not
dust your counter with flour. The recipe requires letting the dough rest
for at least 2 hours before baking. The rolls can be made a day ahead.
To refresh them before serving, wrap them in aluminum foil and heat them
in a 350-degree oven for 15 minutes
