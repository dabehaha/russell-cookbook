Dad's Wings (v1.0)
==================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 lb wings
-  3 tablespoons butter
-  1 jar Durkee Salad Dressing
-  3/4 of a quarter cup of siracha (add more if you prefer spicier
   sauce)
-  Olive Oil (enough to thoroughly fully coat the wings)
-  Salt and Pepper

Procedure
---------

1. Marinate wings with olive oil, salt and pepper in a large zip lock
   bag (preferably overnight).
2. Heat grill to medium heat set up for indirect heat.
3. Place wings on the cool zone of grill and cover. Cook for 25 minutes.
4. Flip the wings and cook an additional 20 minutes covered.
5. Remove wings from grill. 10 minutes before removing the wings from
   the grill, melt the butter over medium heat in a small pot.
6. Once the butter is melted, add the Durkee Salad Dressing and siracha.
   Mix well.
7. Turn the heat down to medium low and simmer, stirring continuously.
   Try to make sure the sauce does not boil.
8. Once the wings are removed from the grill, put them in a casserole
   dish and pour sauce over wings. Serve immediately.
