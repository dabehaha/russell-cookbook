Buckeye Balls (v1.0)
====================

| Created By: Unknown
| Created On: Unknown
| Servings: 72
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 (16 oz) jars peanut butter
-  1-3/4 (16 oz) packages confectioners sugar
-  1/2 cup butter - melted
-  1-1/2 (12 oz) packages semi sweet chocolate chips
-  1 tablespoon shortening

Procedure
---------

1. Blend together the peanut butter, sugar, and melted butter. Allow to
   chill in refrigerator.
2. Roll into 1 inch round calls and return to refrigerator.
3. In a double boiler over medium heat, melt the chocolate and
   shortening. Whisk together until smooth.
4. With a toothpick inserted in the balls, dip them into the chocolate
   so they look like buckeyes. Place on waxed paper and allow to set in
   refrigerator.
