Pasta Con Broccoli (v1.0)
=========================

| Created By: Unknown
| Created On: Unknown
| Servings: 1-2
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  4 ounces cavatelli noodles
-  8 ounces half and half
-  1 ounce butter
-  teaspoon butter
-  1 ounce tomato sauce
-  1 ounce broccoli
-  salt and pepper
-  1 ounce sliced mushrooms
-  cup freshly grated parmigiano cheese

Procedure
---------

1. Cook noodles until half done; strain off water and put noodles back
   into pot.
2. Add half & half, butter, garlic, tomato sauce, and broccoli; bring to
   a hard boil.
3. When noodles are fully cooked; add mushrooms and stir into noodles.
4. Remove from heat; add parmigiano cheese. Toss and serve.
