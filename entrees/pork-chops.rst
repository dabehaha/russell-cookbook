Pork Chops (v1.0)
=================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: 2 hour 15 minutes

Ingredients
-----------

-  1/2 cup water
-  1/3 cup light soy sauce
-  1/4 cup vegetable oil
-  3 tablespoons lemon pepper seasoning
-  2 teaspoons minced garlic
-  6 boneless pork loin chops, trimmed of fat

Procedure
---------

1. Mix water, soy sauce, vegetable oil, lemon pepper seasoning, and
   minced garlic in a deep bowl; add pork chops and marinate in
   refrigerator at least 2 hours.
2. Preheat an outdoor grill for medium-high heat and lightly oil the
   grate.
3. Remove pork chops from the marinade and shake off excess. Discard the
   remaining marinade.
4. Cook the pork chops on the preheated grill until no longer pink in
   the center, 5 to 6 minutes per side. An instant-read thermometer
   inserted into the center should read 145 degrees F (63 degrees C).
