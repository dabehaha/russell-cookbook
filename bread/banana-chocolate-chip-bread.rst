Banana Chocolate Chip Bread (v1.0)
==================================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 cup sugar
-  1 egg
-  1/2 cup butter
-  1 cup mashed ripe bananas
-  3 tablespoon milk
-  2 cups sifted flour
-  1 teaspoon baking powder
-  1/2 teaspoon baking soda
-  1 cup chocolate chips
-  1/2 cup finely chopped pecans

Procedure
---------

1.  Preheat oven to 350 degrees.
2.  Cream sugar, egg, and butter together in large mixer bowl. Beat
    until fluffy. Set aside.
3.  Combine bananas and milk in small bowl. Set aside.
4.  Sift flour, baking powder, and baking soda together in small bowl.
5.  Stir by hand into reserved creamed mixture alternately with reserved
    banana mixture until flour is just moistened.
6.  Stir in chocolate chips and pecans.
7.  Grease 1 (9x5x3 inch) loaf pan.
8.  Turn batter into pan.
9.  Bake for 1 hour or until toothpick inserted in center comes out
    clean.
10. Cool in pan for 10 minutes.
11. Remove from pan. Cool on rack.
