Sides
=====

.. toctree::
   :glob:
   :maxdepth: 1

   ./broccoli-and-rice
   ./funeral-potatoes.rst
   ./grandpa-bryants-dressing
   ./green-bean-casserole
   ./rich-and-charlies-salad
   ./twice-baked-potatoes
   ./zucchini-saute
