Grilled Salmon (v1.0)
=====================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: 2 hours 31 minutes

Ingredients
-----------

-  1-1/2 pound salmon fillets
-  lemon pepper to taste
-  garlic powder to taste
-  salt to taste
-  1/3 cup soy sauce
-  1/3 cup brown sugar
-  1/3 cup water
-  1/4 cup vegetable oil

Procedure
---------

1. Season salmon fillets with lemon pepper, garlic powder, and salt.
2. In a small bowl, stir together soy sauce, brown sugar, water, and
   vegetable oil until sugar is dissolved.
3. Place fish in a large resealable plastic bag with the soy sauce
   mixture, seal, and turn to coat.
4. Refrigerate for at least 2 hours.
5. Preheat grill for medium heat.
6. Lightly oil grill grate.
7. Place salmon on the preheated grill, and discard marinade.
8. Cook salmon for 6 to 8 minutes per side, or until the fish flakes
   easily with a fork.
