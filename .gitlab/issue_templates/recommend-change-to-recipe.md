*Please use this issue if you are recommending a change to a recipe. This template may be used to change a procedure or ingredients. If you are simply recommending an editorial change, please use recipe-correction template.*

*When making your recommendation, it is advised that you navigate to the existing recipe in the repo, click the "open raw" button, and copy and paste the section you would like to change here. Once you have finished the copy and paste, make the changes you would like to the recipe.*

# Summary of Recommended Changes

*The star indicates a field that is required to be filled out.*

**Recipe**\*:

**Changes**\*:
