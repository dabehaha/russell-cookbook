Grandpa Bryant's Dressing (v1.0)
================================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  4-5 loaves of bread (leave out to stale then break into pieces)
-  Crisco
-  5 chicken bouillon cubes
-  1 pound of chicken gizzards and hearts
-  1 pound of livers
-  1/2 teaspoon salt
-  Pepper to taste
-  1/2 handful of parsley
-  4-5 stalks of celery
-  3 large onions
-  milk
-  5 eggs

Procedure
---------

1. Preheat oven to 350 degrees.
2. Melt a half inch of Crisco in a large pan.
3. Take 5 chicken bouillon cubes in a coffee cup with HOT water and set
   aside.
4. Grind chicken gizzards, hearts and livers together.
5. Add meat to hot grease and brown. Add salt, pepper and parsley to
   meat and simmer.
6. Grind celery and onion and add to meat mixture. Let simmer and then
   add bouillon to meat.
7. Add of 1/2 meat mixture to bread, add milk, add rest of meat - keep
   adding milk until smooth. Beat 5 eggs and add to meat mixture. If
   necessary, add more milk should not be sticky.
8. Put in greased bowl and bake for at least 1 hour.
