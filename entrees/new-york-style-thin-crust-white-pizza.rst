New York Style Thin Crust White Pizza (v1.0)
============================================

| Created By: Unknown
| Created On: Unknown
| Servings: 4-6 (two 13-inch pizzas)
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

Dough
^^^^^

-  3 cups (16.5 oz) bread flour
-  2 teaspoons sugar
-  1/2 teaspoon instant or rapid rise yeast
-  1-1/3 cups ice water
-  1 tablespoon vegetable oil
-  1-1/2 teaspoons salt

White Sauce
^^^^^^^^^^^

-  1 cup whole milk ricotta cheese
-  1/4 cup extra virgin olive oil
-  1/4 heavy cream
-  1 large egg yolk
-  4 garlic cloves, minced
-  2 teaspoons minced fresh oregano
-  1 teaspoon minced fresh thyme
-  1/2 teaspoon salt
-  1/4 teaspoon pepper
-  1/8 teaspoon cayenne pepper
-  2 scallions, sliced thin, dark green tops reserved for garnish

Bringing It All Together
^^^^^^^^^^^^^^^^^^^^^^^^

-  1 ounce pecorino cheese, grated fine (1/2 cup)
-  8 ounces whole milk mozzarella cheese, shredded (2 cups)
-  1/2 cup whole milk ricotta cheese

Procedure
---------

Dough
^^^^^

1. Pulse flour, sugar, and yeast in food processor (fitted with dough
   blade if possible) until combined, about 5 pulses.
2. With food processor running, slowly add water; process until dough is
   just combined and no dry flour remains, about 10 seconds.
3. Let dough sit for 10 minutes.
4. Add oil and salt to dough and process until dough forms satiny,
   sticky ball that clears sides of bowl, 30 to 60 seconds.
5. Transfer dough to lightly oiled counter and knead briefly by hand
   until smooth, about 1 minute.
6. Shape dough into tight ball and place in large, lightly oiled bowl;
   cover bowl tightly with plastic wrap and refrigerate for at least 24
   hours and up to 3 days.

White Sauce
^^^^^^^^^^^

1. Whisk all ingredients except scallion greens together in bowl
2. Refrigerate until ready to use.

Bringing It All Together
^^^^^^^^^^^^^^^^^^^^^^^^

1.  One hour before baking, adjust oven rack to upper-middle position
    (rack should be about 4 to 5 inches from broiler), set baking stone
    on rack, and heat oven to 500 degrees.
2.  Transfer dough to clean counter and divide in half.
3.  With cupped palms, form each half into smooth, tight ball.
4.  Place balls of dough on lightly greased baking sheet, spacing them
    at least 3 inches apart; cover loosely with greased plastic and let
    sit for 1 hour.
5.  Coat 1 ball of dough generously with flour and place on well-
    floured counter (keep other ball covered).
6.  Use fingertips to gently flatten dough into 8 inch disk, leaving 1
    inch of outer edge slightly thicker than center.
7.  Using hands, gently stretch disk into 12 inch round, working along
    edges and giving disk quarter turns.
8.  Transfer dough to well floured pizza peel and stretch into 13 inch
    round. Using back of spoon or ladle, spread 1/2 cup ricotta sauce in
    thin layer over surface of dough, leaving 1/4 inch border about
    edge. Sprinkle 1/4 cup Pecorino evenly over sauce, followed by 1 cup
    mozzarella. Dollop 1/4 cup ricotta in teaspoon amounts evenly over
    pizza.
9.  Slide pizza carefully onto baking stone and bake until crust is well
    browned and cheese is bubbly and beginning to brown, 10 to 12
    minutes, rotating pizza halfway through baking.
10. Transfer pizza to wire rack and let cool for 5 minutes before
    slicing and serving. Repeat to shape, top, and bake second pizza.

Notes
-----

If you don’t have a baking stone, bake the pizza on an overturned and
preheated rimmed baking sheet. You can shape the second dough round
while the first pizza bakes, but don’t add the toppings until just
before baking. You will need a pizza peel for this recipe. It is
important to use ice water in the dough to prevent overheating in the
food processor. Semolina flour is ideal for dusting the peel; using it
in place of bread flour if you have it. The sauce will yield more than
needed in the recipe; extra sauce can be refrigerated for up to 1 week
or frozen for up to 1 month.

Variations
^^^^^^^^^^

Use `deep dish red sauce <./chicago-style-deep-dish-pizza.html>`__
as an alternative.
