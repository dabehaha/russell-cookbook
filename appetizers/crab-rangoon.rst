Crab Rangoon (v1.0)
===================

| Created By: Mable
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 cans crab meat, drained (be careful when buying crab meat - NO
   LEGS, white is OK)
-  2 - 8oz packages of cream cheese softened (do **NOT** melt)
-  1/4 teaspoon garlic poweder
-  4 tablespoons parmesan cheese
-  1/4 teaspoon worcestershire sauce
-  1-1/2 lb wonton skins (egg roll skins)

Procedure
---------

1. Mix all ingredients (except wonton skins).
2. Put a scant teaspoon of mixture in center of skin. Moisten fingers
   and fold skin and seal.
3. Fry in 1 inch of cooking oil until golden brown. (watch they brown
   quickly)
4. Cool thoroughly on paper towels.

Notes
-----

Serving
^^^^^^^

Serve with sweet and sour sauce for dipping.

Storage
^^^^^^^

Can freeze in plastic bags.

If reheating, preheat oven to 350 degrees F. Place Crab Rangoon on
cookiesheet and heat in oven for 15 minutes.
