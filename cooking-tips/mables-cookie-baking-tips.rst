Mable's Cookie Baking Tips
==========================

These tips are directly from Mable. The headers were applied by Chris J.
Russell to help easily reference a tip.

Soften Butter
-------------

Do not let butter soften too much. The dough will be too sfot. To
instantly soften butter strike a stick of cold butter with a rolling pin
several times. This is the desired softened consistency.

Flour Choice
------------

I use both Gold Medal and Pillsbury flour. Pillsbury ONLY for Pillsbury
recipes.

Cookie Prep
-----------

I refrigerate all cookie doughs so I can bake and only bake (no mixing
of batter) usually in evenings when no interference. (Also prevents
cookies from spreading.) Check cookies at earliest time designated.

Temperatures While Baking
-------------------------

Use thermometer in your oven (for all uses). Always use a COLD cookie
sheet. I run it under cold water and dry very well between each baking.
Otherwise your cookies will spread before baking

Let cookies cool completely before storing

Margarine Use
-------------

If you decide to use margarine in cookie recies make sure that the
margarine you select contains at least 70% fat. If not your cookies will
flop. Stick margarine only.

Shortening Use
--------------

The only shortening I use is butter-flavored Crisco - the best for
making chocolate chip cookies.
