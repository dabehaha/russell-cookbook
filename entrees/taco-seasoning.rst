Taco Seasoning (v1.0)
=====================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 tablespoon chili powder
-  1 teaspoon garlic powder
-  1 teaspoon onion powder
-  crushed red pepper flakes (optional)
-  1 teaspoon dried oregano
-  1 teaspoon paprika
-  1 teaspoon ground cumin
-  1 teaspoon sea salt
-  1 teaspoon black pepper

Procedure
---------

1. In a small bowl mix together ingredients.

Notes
-----

Storage
^^^^^^^

Store in airtight container.
