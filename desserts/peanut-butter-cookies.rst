Peanut Butter Cookies (v1.0)
============================

| Created By: Unknown
| Created On: Unknown
| Servings: 48 two inch cookies
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  8 tablespoons soft unsalted butter
-  1/2 teaspoon vanilla extract
-  1/2 cup granulated sugar
-  1/2 cup brown sugar
-  1/2 teaspoon salt
-  1/2 teaspoon baking soda
-  1 large egg
-  1 cup creamy peanut butter
-  1-1/2 cup unbleached all purpose flour

Procedure
---------

1. Preheat oven to 350 degrees. Lightly grease 2 baking sheets.
2. In a mixing bowl cream together butter and vanilla.
3. Stir together sugar and brown sugar, pressing out any lumps with the
   back of the spoon, and then gradually cream into butter mixture.
4. Beat in, in this order, salt, baking soda, egg, and peanut butter.
5. Stir in flour.
6. Roll dough into 1 inch balls and place on the baking sheets about 3
   inches apart.
7. When one sheet is covered with balls, press each ball flat with a
   fork that has been dipped in flour.
8. Bake for 12 minutes, or until lightly browned on edges. Cool on wire
   racks.

Notes
-----

Variations
^^^^^^^^^^

Add 1 cup of chocolate chips, if you have a chocolate hankering
