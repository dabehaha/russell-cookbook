Hot Artichoke Dip (v1.0)
========================

| Created By: Unknown
| Created On: Unknown
| Servings: 10-15
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 jars (6 oz each) marinated artichoke hearts
-  1 package (10 oz) frozen chopped spinach
-  1 pressed garlic clove
-  1/2 cup sour cream
-  1/2 cup mayonnaise
-  3/4 cup grated parmesan cheese

Procedure
---------

1. Preheat oven to 375 degrees.
2. Drain and coarsely chop artichoke hearts.
3. Follow instructions for spinach on package and drain well.
4. Mix artichoke hearts and spinach with remaining ingredients.
5. Bake 20-25 minutes or until heated through.
6. Serve with Triscuits or fresh vegetables.
