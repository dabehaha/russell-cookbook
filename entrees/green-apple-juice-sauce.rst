Green Apple Juice Sauce (v1.0)
==============================

| Created By: Unknown
| Created On: Unknown
| Servings: 1 Cup
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  2 tablespoons unsalted butter
-  2 jalapeños, coarsely chopped
-  1 medium onion, coarsely chopped
-  2 cups chicken stock
-  1-1/2 cups frozen green apple juice concentrate (12 ounce can)
-  3 tablespoons light brown sugar
-  salt and freshly ground pepper

Procedure
---------

1. In a medium saucepan over medium high heat, melt the butter and sweat
   the jalapeños and onion.
2. Add the stock, apple juice concentrate, and brown sugar and cook
   until reduced to half, about 30 minutes.
3. Season to taste with salt and pepper.

Notes
-----

May be made up to 2 days ahead and refrigerated and covered. Reheat,
stirring, just before serving.
