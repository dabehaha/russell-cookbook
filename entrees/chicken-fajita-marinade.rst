Chicken Fajita Marinade (v1.0)
==============================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1/4 cup fresh lime juice
-  1/3 cup water
-  2 tablespoons vegetable oil
-  1 large clove garlic, pressed
-  3 teaspoons vinegar
-  2 teaspoons soy sauce
-  1/2 teaspoon liquid smoke
-  1 teaspoon salt
-  1/2 teaspoon chili powder
-  1/2 teaspoon cayenne pepper
-  1/4 teaspoon ground black pepper
-  Dash onion powder

Procedure
---------

1. Mix all ingredients together.

Notes
-----

Marinade makes enough for 2 chicken breasts.
