Baked Potato Soup (v1.0)
========================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

Soup
^^^^

-  2 medium potatoes (Dad uses more)
-  3 tablespoons butter
-  1 cup diced white onion
-  2 tablespoons flour
-  4 cups chicken stock
-  2 cups water
-  1/4 cup cornstarch
-  1-1/2 cups instant mashed potatoes
-  1 teaspoon salt
-  3/4 teaspoon pepper
-  1/2 teaspoon basil
-  1/8 teaspoon thyme
-  1 cup half & half

Garnish
^^^^^^^

-  1/2 cup shredded cheddar cheese
-  1/4 cup crumbled cooked bacon
-  2 green onions, chopped

Procedure
---------

1.  Preheat oven to 400 degrees and bake the potatoes for 1 hour or
    until done.
2.  When potatoes have cooked remove them from the oven to cool.
3.  As potatoes cool prepare soup by melting butter in a large saucepan,
    and saute onion until light brown.
4.  Add the flour to the onions and stir to make a roux.
5.  Add stock, water, cornstarch, instant mashed potatoes, and spices to
    the pot and bring to a boil.
6.  Reduce heat and summer for 5 minutes.
7.  Cut potatoes in half length wise and scoop out contents with a large
    spoon. Discard skin.
8.  Chop baked potato with a large knife to make chunks that are about
    1/2 inch in size.
9.  Add chopped baked potato and half & half to the saucepan, bring soup
    back to a boil then reduce heat and simmer the soup for another 15
    minutes or until thick.
10. Spoon about 1 1/2 cups of soup into a bowl and top with garnishes.
