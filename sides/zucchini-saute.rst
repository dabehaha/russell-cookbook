Zucchini Saute (v1.0)
=====================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1 tablespoon olive oil
-  1/2 teaspoon minced garlic
-  3 large zucchini squash, thinly sliced
-  1/2 teaspoon salt
-  1/4 teaspoon pepper
-  1/4 cup grated Parmesan cheese

Procedure
---------

1. Heat the olive oil in a medium skillet over medium heat.
2. When hot, add the garlic and saute for 2 minutes, or until fragrant;
   don't let it brown.
3. Add the squash, salt and pepper and cook until the squash is tender
   but still slightly crisp, about 5 minutes.
4. Transfer the squash to a serving dish and sprinkle with the Parmesan
   cheese.
