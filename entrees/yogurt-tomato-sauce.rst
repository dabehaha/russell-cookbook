Yogurt-Tomato Sauce (v1.0)
==========================

| Created By: Unknown
| Created On: Unknown
| Servings: Unknown
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1/2 pint Greek yogurt
-  2 cloves garlic, finely chopped
-  1/4 cup finely grated peeled cucumber
-  1 plum tomato, seeded and finely diced
-  1 tablespoon capers, drained
-  2 tablespoons finely chopped fresh dill
-  1/2 tablespoon red wine vinegar
-  kosher salt and freshly ground black pepper

Procedure
---------

1. Stir together the yogurt, garlic, cucumber, tomato, capers, dill,
   vinegar, and salt and pepper to taste in a medium bowl.
2. Cover and let sit in the refrigerator for at least 30 minutes and up
   to 4 hours before serving.
