Meatballs (v1.0)
================

| Created By: Unknown
| Created On: Unknown
| Servings: 8
| Prep. Time: Unknown
| Cook Time: 30 minutes

Ingredients
-----------

-  1 pound ground beef
-  1/2 pound ground veal
-  1/2 pound ground pork
-  2 cloves garlic, minced
-  2 eggs
-  1 cup freshly grated Romano cheese
-  1-1/2 tablespoons chopped Italian flat leaf parsley
-  salt and ground black pepper to taste
-  2 cups stale Italian bread, crumbled
-  1-1/2 cups lukewarm water
-  1 cup olive oil

Procedure
---------

1. Preheat oven to 400
2. Combine beef, veal, and pork in a large bowl. Add garlic, eggs,
   cheese, parsley, salt and pepper.
3. Blend bread crumbs into meat mixture. Slowly add the water 1/2 cup at
   a time. The mixture should be very moist but still hold its shape if
   rolled into meatballs. (I usually use about 1 1/4 cups of water).
4. Shape into meatballs.
5. Bake in oven 20 - 25 minutes.
