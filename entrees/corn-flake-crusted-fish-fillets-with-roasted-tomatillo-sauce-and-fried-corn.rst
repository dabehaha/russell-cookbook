Corn Flake - Crusted Fish Fillets with Roasted Tomatillo Sauce and Fried Corn (v1.0)
====================================================================================

| Created By: Unknown
| Created On: Unknown
| Servings: 6
| Prep. Time: Unknown
| Cook Time: Unknown

Ingredients
-----------

-  1/2 cup all purpose flour
-  Salt
-  2 large eggs
-  One 7 ounce box of corn flakes
-  Six 5 to 6 ounce skinless fish fillets (choose snapper, grouper,
   halibut, striped bass, mahi mahi, or other medium flake light
   flavored fish), about 3/4 inch thick
-  1 pound (10 to 12 medium) tomatillos, husked and rinsed
-  Fresh hot green chiles to taste (roughly 3 serranos or 1 jalapeño),
   stemmed
-  2 tablespoons olive oil
-  1 medium white onion, sliced
-  3 large garlic cloves, peeled and finely chopped
-  2 cups light fish or chicken broth
-  2/3 cup loosely packed chopped fresh cilantro, plus a few sprigs for
   garnish
-  2 cups fresh corn kernels (you’ll need about 3 ears of sweet corn or,
   for the most traditional Mexican flavor, 2 ears of field corn at the
   milk stage - what we called roasting ears when I was a kid)
-  Vegetable oil to a depth of 1/4 inch for panfrying

Procedure
---------

Breading The Fish
^^^^^^^^^^^^^^^^^

1. Spread the flour on a deep plate (or pie plate), then stir in 1/2
   teaspoon salt.
2. Break the eggs onto another deep plate and add 3 tablespoons water
   and 1/2 teaspoon salt.
3. Beat with a fork until completely liquid.
4. Spread the corn flakes on a third plate, then use the back of a
   measuring cup to gently break them into 1/4 inch pieces.
5. Dredge all sides of 1 fish fillet in the flour, then lay it in the
   egg mixture.
6. Use a large fork to flip it over, then carefully transfer the drippy
   piece of fish to the plate of corn flakes.
7. Sprinkle flakes from the dish over the top of the fish and press them
   firmly; the fish should be thoroughly coated with flakes.
8. Transfer to another plate or baking sheet, then “bread” the remaining
   fillets.
9. Refrigerate uncovered for at least 1 hour, or up to 6 hours.

Sauce
^^^^^

1.  Roast tomatillos and chiles on a baking sheet 4 inches below a very
    hot broiler until darkly roasted, even blackened in spots, about 5
    minutes.
2.  Flip them over and roast the other side - 4 to 5 minutes more will
    give you splotchy black and blistered tomatillos and holes that are
    soft and cooked through.
3.  Cool, then transfer everything to a food processor, being careful to
    scrape up all the delicious juice that has run out onto the baking
    sheet.
4.  Process until smoothly pureed.
5.  Set a heavy medium (4 qt) saucepan over medium heat and measure in 1
    tablespoon of the olive oil.
6.  When hot, add the onion and cook, stirring regularly, until richly
    golden, about 7 minutes.
7.  Stir in the garlic and cook for a minute longer.
8.  Raise the heat to medium-high, and, when the oil is really sizzling,
    add the tomatillo puree all at once.
9.  Stir until noticeably darker and very thick, 3 to 4 minutes.
10. Add the broth and 1/3 cup of the cilantro.
11. Stir everything thoroughly.
12. Simmer, stirring often, over medium-low heat until the flavors
    mellow and the consistency thickens enough to coat a spoon (but not
    too heavily) about 30 minutes.
13. Taste and season with salt, usually about 3/4 teaspoon.
14. Keep warm over low heat.

Bringing It All Together
^^^^^^^^^^^^^^^^^^^^^^^^

1.  In a medium skillet, heat the remaining 1 tablespoon olive oil over
    medium.
2.  When hot, add the corn and stir frequently until nicely browned, 5
    to 10 minutes.
3.  Sweet corn will be a little chewy, field corn will be quite chewy,
    meaning you may want to dribble a little water in the pan to steam
    the kernels to a bit more tenderness. Set aside in the pan.
4.  Turn on the oven to the lowest setting.
5.  Heat 1/4 inch of vegetable oil in a large heavy skillet over medium
    to medium-high.
6.  When the oil is hot enough to make a edge of “breaded” fillet really
    sizzle, fry the fillets in two batches. (They shouldn’t be crowded
    in the pan, or they won’t crust and brown nicely.)
7.  They’ll need to cook about 2 minutes per side to brown and be done
    enough to flake under firm pressure - it takes a little practice to
    check this without breaking the crust very much.
8.  Carefully transfer the first batch of cooked fillets to a paper
    towel - lined baking sheet and keep warm in the oven while you’re
    frying you’re the second batch.
9.  Spoon the warm sauce (thin it with a little water if it has
    thickened) onto a deep warm platter and arrange the crusty fish
    fillets slightly overlapping down the center.
10. Sprinkle the whole affair with the corn (reheat if it has cooled
    off) and the remaining 1/3 cup cilantro.
11. Garnish with the cilantro sprigs and you’re ready to make your
    triumphant entrance to the dining room.

Notes
-----

Though this restauranty dish may be more than you want to tackle just
before guests sit down, you can easily do it in stages. Make the sauce
and the corn a day ahead (store them separately, covered, in the
refrigerator), then rewarm both just before serving. Bread the fish up
to 6 hours ahead then fry it just before you’re ready to serve.
